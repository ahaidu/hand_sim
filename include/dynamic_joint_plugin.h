#ifndef DYNAMIC_JOINT_PLUGIN_HH
#define DYNAMIC_JOINT_PLUGIN_HH

#include "gazebo.hh"
#include "physics/physics.hh"
#include "common/Events.hh"
#include "common/Plugin.hh"
#include "physics/SurfaceParams.hh"
#include <boost/bind.hpp>
#include <boost/thread.hpp>
#include <ros/ros.h>
#include <sstream>

#include <pcl/point_cloud.h>
#include <pcl/common/centroid.h>
#include <pcl/ros/conversions.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/kdtree/kdtree_flann.h>

#include <Eigen/Core>

#include <hand_sim/HydraRaw.h>
#include <hand_sim/ThreegearGesture.h>

namespace gazebo
{

  class DynamicJointPlugin : public WorldPlugin
  {

  	public: DynamicJointPlugin();

    public: virtual ~DynamicJointPlugin();

    protected: virtual void Load(physics::WorldPtr _parent, sdf::ElementPtr _sdf);

    protected: virtual void OnUpdate();

    private:
    	void HydraDynamicJointCallback(const hand_sim::HydraRaw &msg);
    	void ThreegearDynamicJointCallback(const hand_sim::ThreegearGesture &msg);
    	void initModels();
    	void setSDFParameters(const sdf::ElementPtr _sdf);
    	void createPancake();
    	void createJoint(physics::LinkPtr anchor_link, physics::LinkPtr ext_link);
    	void initLiquidCloud(pcl::PointCloud<pcl::PointXYZ> &cloud);
    	void getCloudCenter(pcl::PointCloud<pcl::PointXYZ> &cloud, pcl::PointXYZ &center_pos);
    	unsigned int GetClosestPoint(pcl::PointCloud<pcl::PointXYZ> &cloud, pcl::PointXYZ pos);
    	void CookingPancakeThreadFunc(void);

    private:
		physics::ModelPtr liquid_model;
		physics::WorldPtr world;
		physics::LinkPtr center_link;
		std::vector<physics::JointPtr> myJoints;
 		event::ConnectionPtr updateConnection;
 		ros::Subscriber ros_subscriber;
 		ros::NodeHandle* rosnode;
 		bool jointCreated, jointButtonStatus;
 		double stop_cfm, stop_erp, limit_stop, pancake_inertia, fudge_factor, cfm, erp;
 		std::string type;
 		boost::thread *cooking_thread;
  };
}
#endif
