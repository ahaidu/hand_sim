#ifndef GRIPPER_PLUGIN_HH
#define GRIPPER_PLUGIN_HH

#include "gazebo.hh"
#include "physics/physics.hh"
#include "common/Events.hh"
#include "common/Plugin.hh"
#include "physics/PhysicsTypes.hh"

namespace gazebo
{

  class GripperPlugin : public ModelPlugin
  {

  	public:
	  GripperPlugin();
	  virtual ~GripperPlugin();

  	protected:
	  virtual void Load(physics::ModelPtr _parent, sdf::ElementPtr /*_sdf*/);
  	  virtual void OnUpdate();
  	  virtual void Init();

  	private:
  	  void ThreegearGestureCallback(const hand_sim::ThreegearGesture &msg);
  	  void onContact(const std::string &_collisionName, const physics::Contact &_contact);
  	  void setGripperMode();
  	  void createJoint();
  	  void destroyJoint();
  	  void setHandJoints();
  	  void setHandJointsForces();
  	  void initJointsPosition();
  	  void setJointsPosition();

    private:
		ros::Subscriber threegear_gesture_subscriber;
		ros::NodeHandle* rosnode;
		physics::ModelPtr hand_model, grabbed_model;
		physics::WorldPtr world;
 		event::ConnectionPtr updateConnection;
 		common::Time prevUpdateTime;

 		physics::JointPtr thumb_base_joint, fixed_joint;
 		std::vector<physics::JointPtr> hand_joints;
 		std::vector<double> jointPositions;
 		std::vector<common::PID> jointPIDs;

 		std::vector<physics::CollisionPtr> collisions;
 		std::vector<event::ConnectionPtr> connections;

 		int gesture; /*  1 PRESSED  /  2 DRAGGED  /  3 RELEASED */
 		int gripper_mode; /*  -1 KEEP CURR STATE /  0 OFF  /  1 ON */
 		bool joint_created, thumb_contact, fore_contact;
 		double joint_position;
  };
}
#endif
