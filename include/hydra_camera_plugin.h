#ifndef HYDRA_CAMERA_PLUGIN_HH
#define HYDRA_CAMERA_PLUGIN_HH

#include "gazebo.hh"
#include "rendering/UserCamera.hh"
#include "rendering/Scene.hh"
#include "rendering/RenderEngine.hh"
#include "gui/Gui.hh"
#include "gazebo/rendering/Visual.hh"


#include <OGRE/OgreRoot.h>

#include <hand_sim/HydraRaw.h>

#include <boost/bind.hpp>
#include <boost/thread.hpp>
#include <ros/ros.h>


namespace gazebo
{

  class HydraCameraPlugin : /*public WorldPlugin*/ public SystemPlugin
  {

  	public:
	  HydraCameraPlugin();

	  virtual ~HydraCameraPlugin();

	  virtual void Load(int /*_argc*/, char ** /*_argv*/);

	  void Init();

    private:
	  void HydraCallback(const hand_sim::HydraRaw& msg);

	  void spinnerFunc(void);

    private:
	  ros::Subscriber hydra_subscriber;
	  ros::NodeHandle* rosnode;
	  rendering::UserCameraPtr activeCam;
	  rendering::VisualPtr refVisual;
	  math::Vector3 refCameraPos, focalPoint;
	  math::Pose cameraPose;
	  bool reference_pos_set, flags_set;

	  boost::thread *spinner_thread;
  };
}
#endif
