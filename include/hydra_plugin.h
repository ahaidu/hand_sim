#ifndef HYDRA_PLUGIN_HH
#define HYDRA_PLUGIN_HH

#include "gazebo.hh"
#include "physics/physics.hh"
#include "common/Events.hh"
#include "common/Plugin.hh"
#include "common/Time.hh"

#include <boost/bind.hpp>
#include <ros/ros.h>

#include <hand_sim/HydraRaw.h>

#include "gazebo/transport/TransportTypes.hh"
#include "gazebo/msgs/MessageTypes.hh"

namespace gazebo
{   
  class HydraPlugin : public ModelPlugin
  {

  	public: HydraPlugin();

    public: virtual ~HydraPlugin();

    protected: virtual void Load(physics::ModelPtr _parent, sdf::ElementPtr _sdf);
   
    protected: virtual void OnUpdate();

    private: 
    	void HydraCallback(const hand_sim::HydraRaw &msg);
    	void InitPid(double P, double I, double D, double I1_max,  double I2_min);
    	void HydraInit();
    	void ControlPose(const hand_sim::HydraRaw& msg);
    	math::Vector3 ReturnRotVelocity();

    	void setHandJoints();
    	void closeGripper(bool close_flag);
    	void openGripper();
    	void attachJoint(const physics::LinkPtr);
    	void detachJoint();
    	void LogTimestamp(std::string msg);
    	void OnContact(ConstContactsPtr &_msg/*const std::string &_collisionName, const physics::Contact &_contact*/);
    	void GetSdfParameters(const sdf::ElementPtr &_sdf);
    	void StartLogging();
    	void StopLogging();
    	void PauseWorld();

    private:
    	double precision;
 		ros::Subscriber hydra_subscriber;
 		ros::NodeHandle* rosnode;
		physics::ModelPtr hand_model;
 		event::ConnectionPtr updateConnection;
 		common::Time time_of_last_cycle;
 		common::PID pid_controller_x, pid_controller_y, pid_controller_z;
 		bool hydra_on, hydraOnOffButtonState, hydraPauseButtonState;
 		math::Pose curr_pose;
 		math::Vector3 curr_position, desired_position, offset_position, pid_lin_velocity, rot_velocity;
 		math::Quaternion curr_quatern, desired_quatern, hydra_offset_q, hydra_slide_offset_q;

 	    /// \brief Used to start, stop, and step simulation.
 	    private: transport::PublisherPtr worldControlPub;

 	    /// \brief Node used for communication.
 	    private: transport::NodePtr node;

 		std::vector<physics::JointPtr> hand_joints;

 		physics::WorldPtr world;
 		physics::JointPtr fixed_joint;
 		bool gripper_button_pressed, fixed_joint_attached;

 		// flag for logging the data or not
 		int logging_on;

 		// flag for logging the data or not
 		bool curr_logging;

		transport::SubscriberPtr contactSub;
		transport::NodePtr contactNode;

		sensors::ContactSensorPtr contactSensor;


  };
}
#endif
