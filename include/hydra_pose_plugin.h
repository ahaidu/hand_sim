#ifndef HYDRA_POSE_PLUGIN_HH
#define HYDRA_POSE_PLUGIN_HH

#include "gazebo.hh"
#include "physics/physics.hh"
#include "common/Events.hh"
#include "common/Plugin.hh"

#include <boost/bind.hpp>
#include <ros/ros.h>

#include <hand_sim/HydraRaw.h>

namespace gazebo
{   
  class HydraPosePlugin : public ModelPlugin
  {

  	public: HydraPosePlugin();

    public: virtual ~HydraPosePlugin();

    protected: virtual void Load(physics::ModelPtr _parent, sdf::ElementPtr _sdf);
   
    protected: virtual void OnUpdate();

    private: 
    	void HydraCallback(const hand_sim::HydraRaw &msg);

    	math::Vector3 ReturnRotVelocity();

    private:
 		ros::Subscriber hydra_subscriber;
 		ros::NodeHandle* rosnode;
		physics::ModelPtr myModel;
 		event::ConnectionPtr updateConnection;
 		common::Time time_of_last_cycle;
 		common::PID pid_controller_x, pid_controller_y, pid_controller_z;
 		bool hydra_on, hydraOnOffButtonState, offsetPositionSet;;
 		math::Pose curr_pose;
 		math::Vector3 curr_position, desired_position, offsetPosition, pid_lin_velocity, rot_velocity;
 		math::Quaternion curr_quatern, desired_quatern, hydra_offset_q;

  };
}
#endif
