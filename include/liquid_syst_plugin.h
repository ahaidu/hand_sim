#ifndef LIQUID_SYST_PLUGIN_HH
#define LIQUID_SYST_PLUGIN_HH

#include "gazebo.hh"
#include "rendering/UserCamera.hh"
#include "rendering/Scene.hh"
#include "rendering/RenderEngine.hh"
#include "gui/Gui.hh"

#include <OGRE/OgreRoot.h>

#include <boost/bind.hpp>
#include <boost/thread.hpp>

namespace gazebo
{

  class LiquidSystPlugin : public SystemPlugin
  {

  	public:
	  LiquidSystPlugin();

	  virtual ~LiquidSystPlugin();

	  virtual void Load(int /*_argc*/, char ** /*_argv*/);

	  void Init();

    private:
	  void ThreadFunc(void);

    private:
	  math::Pose offsetPose;
	  physics::WorldPtr world;
	  Ogre::Root* mRoot;
	  Ogre::SceneManager* mScenemanager;

	  rendering::CameraPtr camera;
	  rendering::UserCameraPtr activeCam, offsetCam;
	  rendering::ScenePtr mScene;

	  common::Time sleepDuration;

	  boost::thread *thread;

	  math::Quaternion camera_offset_q;
	  math::Pose cameraPose;

	  Ogre::SceneManager *mSceneMgr;


  };
}
#endif
