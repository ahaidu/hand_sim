#ifndef MUG_PLUGIN_HH
#define MUG_PLUGIN_HH

#include "gazebo.hh"
#include "physics/physics.hh"
#include "common/Events.hh"
#include "common/Plugin.hh"

#include <iostream>
#include <fstream>

#include <boost/bind.hpp>
#include <ros/ros.h>

#include <hand_sim/HydraRaw.h>

namespace gazebo
{

  class MugPlugin : public ModelPlugin
  {

  	public: MugPlugin();

    public: virtual ~MugPlugin();

    protected: virtual void Load(physics::ModelPtr _parent, sdf::ElementPtr /*_sdf*/);

    protected: virtual void OnUpdate();

    private:
    	void MugCallback(const hand_sim::HydraRaw &msg);
    	void initModels();
    	void createJoint();
    	void destroyJoint();
    	void logTimestamp(std::string msg);


    private:
		physics::ModelPtr mug_model, rosie_model;
		physics::JointPtr myJoint;
 		event::ConnectionPtr updateConnection;
 		ros::Subscriber hydra_subscriber;
 		ros::NodeHandle* rosnode;
 		bool jointCreated, hydraJointButtonStatus;
  };
}
#endif
