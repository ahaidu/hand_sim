#ifndef MUG_VISUAL_PLUGIN_HH
#define MUG_VISUAL_PLIGIN_HH

#include <gazebo.hh>
#include <rendering/Visual.hh>
#include <common/common.hh>
#include "physics/physics.hh"
#include "gui/Gui.hh"
#include "rendering/Scene.hh"
#include "rendering/UserCamera.hh"

#include <boost/bind.hpp>

namespace gazebo
{
class MugVisualPlugin : public VisualPlugin
{
public:
	void Load(rendering::VisualPtr _parent, sdf::ElementPtr /*_sdf*/);
	void OnUpdate();
	void DrawLine(const math::Vector3 &_start,
				  const math::Vector3 &_end,
				  const std::string &_name,
				  Ogre::SceneManager *_manager,
				  const double &_distance);

private:
	rendering::VisualPtr link_visual;
	event::ConnectionPtr updateConnection;
};
}

#endif
