#ifndef SPATULA_PLUGIN_HH
#define SPATULA_PLUGIN_HH

#include "gazebo.hh"
#include "physics/physics.hh"
#include "common/Events.hh"
#include "common/Plugin.hh"

#include <boost/bind.hpp>
#include <ros/ros.h>

#include <hand_sim/HydraRaw.h>

namespace gazebo
{

  class SpatulaPlugin : public ModelPlugin
  {

  	public: SpatulaPlugin();

    public: virtual ~SpatulaPlugin();

    protected: virtual void Load(physics::ModelPtr _parent, sdf::ElementPtr _sdf);

    protected: virtual void OnUpdate();

    private:
    	void SpatulaCallback(const hand_sim::HydraRaw &msg);
    	void initModels();
    	void createJoint();
    	void destroyJoint();
    	void logTimestamp(std::string msg);

    private:
		physics::ModelPtr spatula_model, rosie_model;
		physics::JointPtr myJoint;
 		event::ConnectionPtr updateConnection;
 		ros::Subscriber hydra_subscriber;
 		ros::NodeHandle* rosnode;
 		double stop_cfm, stop_erp, hi_stop, lo_stop;
 		bool jointCreated, hydraJointButtonStatus;
  };
}
#endif
