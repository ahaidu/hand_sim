#ifndef STEREO_RENDERING_PLUGIN_HH
#define STEREO_RENDERING_PLUGIN_HH

#include "gazebo.hh"
#include "rendering/UserCamera.hh"
#include "rendering/Scene.hh"
#include "rendering/RenderEngine.hh"
#include "gui/Gui.hh"

#include <OGRE/OgreRoot.h>

#include <boost/bind.hpp>
#include <boost/thread.hpp>

#include "StereoManager.h"


namespace gazebo
{

  class StereoRenderingPlugin : public SystemPlugin
  {

  	public:
	  StereoRenderingPlugin();

	  virtual ~StereoRenderingPlugin();

	  virtual void Load(int /*_argc*/, char ** /*_argv*/);

	  void Init();

	  void OnUpdate();

    private:
	  void ThreadFunc(void);

    private:
      event::ConnectionPtr updateConnection;
	  math::Pose offsetPose;
	  physics::WorldPtr world;
	  Ogre::Root* mRoot;
	  Ogre::SceneManager* mScenemanager;

	  rendering::CameraPtr camera;
	  rendering::UserCameraPtr activeCam, offsetCam;
	  rendering::ScenePtr mScene;

	  common::Time sleepDuration;

	  boost::thread *thread;

	  math::Quaternion camera_offset_q;
	  math::Pose cameraPose;

	  StereoManager *mStereoManager;
	  //StereoManager mStereoManagerA;
	  Ogre::SceneManager *mSceneMgr;


  };
}
#endif
