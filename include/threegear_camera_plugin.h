#ifndef THREEGEAR_CAMERA_PLUGIN_HH
#define THREEGEAR_CAMERA_PLUGIN_HH

#include "gazebo/gazebo.hh"
#include "rendering/UserCamera.hh"
#include "gui/Gui.hh"

#include <hand_sim/ThreegearPosition.h>
#include <hand_sim/ThreegearGesture.h>
#include <ros/ros.h>
#include <boost/thread.hpp>
#include <boost/bind.hpp>


namespace gazebo
{

  class ThreegearCameraPlugin : /*public WorldPlugin*/ public SystemPlugin
  {

  	public:
	  ThreegearCameraPlugin();

	  virtual ~ThreegearCameraPlugin();

	  virtual void Load(int /*_argc*/, char ** /*_argv*/);

	  void Init();

    private:
	  void ThreegearPosCallback(const hand_sim::ThreegearPosition& msg);

	  void ThreegearGestureCallback(const hand_sim::ThreegearGesture& msg);

	  void SpinnerFunc(void);


    private:
	  bool pinch_flag, control_flag;
	  ros::Subscriber threegear_pos_subscriber, threegear_gesture_subscriber;
	  ros::NodeHandle* rosnode;
	  rendering::UserCameraPtr activeCam;
	  math::Pose cameraPose;
	  math::Quaternion threegear_offset_q;
	  boost::thread *spinner_thread;


  };
}
#endif
