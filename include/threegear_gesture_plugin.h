#ifndef THREEGEAR_GESTURE_PLUGIN_HH
#define THREEGEAR_GESTURE_PLUGIN_HH

#include "gazebo.hh"
#include "physics/physics.hh"
#include "common/Events.hh"
#include "common/Plugin.hh"

#include <boost/bind.hpp>
#include <ros/ros.h>

#include <hand_sim/ThreegearGesture.h>

namespace gazebo
{

  class ThreegearGesturePlugin : public ModelPlugin
  {

  	public:
	  ThreegearGesturePlugin();
	  virtual ~ThreegearGesturePlugin();

  	protected:
	  virtual void Load(physics::ModelPtr _parent, sdf::ElementPtr /*_sdf*/);
  	  virtual void OnUpdate();
  	  virtual void Init();

  	private:
  	  void ThreegearGestureCallback(const hand_sim::ThreegearGesture &msg);
  	  void OnContact(ConstContactsPtr &_msg);
  	  void attachJoint(const physics::LinkPtr attachLink);
  	  void detachJoint();
  	  void setHandJoints();
  	  void initContactNode();
  	  void CloseGripper(bool close_flag);
  	  void OpenGripper();


    private:
		ros::Subscriber threegear_gesture_subscriber;
		ros::NodeHandle* rosnode;
		physics::ModelPtr hand_model;
		physics::WorldPtr world;
 		event::ConnectionPtr updateConnection;

 		physics::JointPtr thumb_base_joint, fixed_joint;
 		std::vector<physics::JointPtr> hand_joints;

 		int gesture; /*  1 PRESSED  /  2 DRAGGED  /  3 RELEASED */
 		bool fixed_joint_attached, gripper_button_pressed;

 		//For Contact Sensor
 		transport::SubscriberPtr contactSub;
 		transport::NodePtr contactNode;

 		sensors::ContactSensorPtr contactSensor;
  };
}
#endif
