#ifndef THREEGEAR_PLUGIN_HH
#define THREEGEAR_PLUGIN_HH

#include "gazebo.hh"
#include "physics/physics.hh"
#include "common/Events.hh"
#include "common/Plugin.hh"
#include "common/Time.hh"

#include <boost/bind.hpp>
#include <ros/ros.h>

#include <hand_sim/ThreegearPosition.h>

namespace gazebo
{   
  class ThreegearPlugin : public ModelPlugin
  {

  public:
	  ThreegearPlugin();
	  virtual ~ThreegearPlugin();

  protected:
	  virtual void Load(physics::ModelPtr _parent, sdf::ElementPtr /*_sdf*/);
  	  virtual void OnUpdate();

  private:
  	  void ThreegearCallback(const hand_sim::ThreegearPosition &msg);
	  void InitPid(double P, double I, double D, double I1_max,  double I2_min);
	  void ThreegearInit();
	  math::Vector3 returnRotVelocity();

  private:
	  ros::Subscriber threegear_subscriber;
	  ros::NodeHandle* rosnode;
	  physics::ModelPtr hand_model;
	  event::ConnectionPtr updateConnection;
	  common::Time time_of_last_cycle;
	  common::PID pid_controller_x, pid_controller_y, pid_controller_z;
	  math::Pose curr_pose;
	  math::Vector3 curr_position, desired_position, offset_position, pid_lin_velocity, rot_velocity;
	  math::Quaternion curr_quatern, desired_quatern, threegear_offset_q;
  };
}
#endif
