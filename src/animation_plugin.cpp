#include "common/CommonTypes.hh"
#include "common/Animation.hh"
#include "common/KeyFrame.hh"
#include "physics/Model.hh"
#include "gazebo.hh"
#include <ros/ros.h>
#include "hand_sim/ThreegearPosition.h"
#include "hand_sim/HydraRaw.h"
#include <boost/bind.hpp>


namespace gazebo
{
  class AnimationPlugin : public ModelPlugin
  {
    public: void Load(physics::ModelPtr _parent, sdf::ElementPtr _sdf)
    {
    	////////////// get topic param
    	if (!_sdf->HasElement("topic"))
    	{
    		std::cout << "Missing parameter <topic> in DynamicJoint, default to hydra_raw" << std::endl;
    		this->topic = "/threegear_raw";
    	}
    	else this->topic = _sdf->GetElement("topic")->GetValueString();


    	this->model = _parent;

    	this->world = _parent->GetWorld();

    	this->model->SetGravityMode(false);

    	int argc = 0;
    	char** argv = NULL;
    	ros::init(argc,argv,"Threegear_plugin");
    	// ROS Nodehandle
    	this->rosnode = new ros::NodeHandle();


    	// Listen to the update event. This event is broadcast every simulation iteration.
    	this->updateConnection = event::Events::ConnectWorldUpdateBegin(
    			boost::bind(&AnimationPlugin::OnUpdate, this));

    	// ThreeGear Subscriber
    	this->threegear_subscriber = this->rosnode->subscribe(
    			this->topic, 100, &AnimationPlugin::ThreegearCallback, this);

    	ROS_INFO("**********ANIMATION LOADED on topic %s*********", this->topic.c_str());

    }

    public: ~AnimationPlugin()
    {
    	delete this->rosnode;
    }

    public: void OnUpdate()
    {
    	ros::spinOnce();
    }

    private: void ThreegearCallback(/*const hand_sim::HydraRaw& msg*/
    		const hand_sim::ThreegearPosition &msg)
    {

/*    	math::Vector3 desired_position = math::Vector3(
    			- (float) ((float)msg.pos[1] / 200) + 2,
    			- (float) ((float)msg.pos[0] / 200) + 0,
    			- (float) ((float)msg.pos[2] / 200) + 1);*/

    	math::Vector3 desired_position = math::Vector3(
    			msg.pos_right[0],
    			msg.pos_right[1],
    			msg.pos_right[2]);

    	std::cout << desired_position << std::endl;

        gazebo::common::PoseAnimationPtr anim(
            new gazebo::common::PoseAnimation("Animation", 1000.0, false));

        gazebo::common::PoseKeyFrame *key;

        key = anim->CreateKeyFrame(0);
        key->SetTranslation(math::Vector3(desired_position));
        key->SetRotation(math::Quaternion(0, 0, 0));

        key = anim->CreateKeyFrame(0.1);
        key->SetTranslation(math::Vector3(desired_position));
        key->SetRotation(math::Quaternion(0, 0, 0));

        this->model->SetAnimation(anim);

    	//this->model->SetWorldPose(math::Pose(desired_position, math::Quaternion(0,0,0)));

    }

    private:
    	ros::Subscriber threegear_subscriber;
    	ros::NodeHandle* rosnode;
    	physics::ModelPtr model;
    	physics::WorldPtr world;
    	event::ConnectionPtr updateConnection;
    	std::string topic;
  };

  // Register this plugin with the simulator
  GZ_REGISTER_MODEL_PLUGIN(AnimationPlugin)
}
