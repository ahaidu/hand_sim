#include "gazebo/gazebo.hh"
#include "physics/physics.hh"
#include "common/Plugin.hh"
#include "transport/transport.hh"
#include <math.h>

#define PI 3.14159265

namespace gazebo
{
  class FactoryLiquid : public WorldPlugin
  {
  	public: virtual ~FactoryLiquid()
  	{

  	}
    public: void Load(physics::WorldPtr _parent, sdf::ElementPtr _sdf)
    {
    	math::Vector3 p3, init_pos;
    	std::stringstream xml;
    	int spawned, level;
    	unsigned int nr_spheres;
    	double mass, radius, friction, friction2, rolling_friction, cfm, erp, kp, kd,
    	restitution_coefficient, inertia, split_impulse_penetration_threshold, split_impulse;

    	///////////////////////////////////////////////////////////////////////////////////
    	/////// SDF PARAMETERS

    	////////////// Get nr of spheres
        if (!_sdf->HasElement("nr_spheres"))
        {
      	  std::cout << "Missing parameter <nr_spheres> in FactoryLiquid, default to 0" << std::endl;
      	  nr_spheres = 0;
        }
        else nr_spheres = _sdf->GetElement("nr_spheres")->GetValueUInt();

    	////////////// Set up the initial position parameter
        if (!_sdf->HasElement("init_pos"))
        {
      	  std::cout << "Missing parameter <init_pos> in FactoryLiquid, default to 0 0 0" << std::endl;
      	  init_pos.x = 0.0;
      	  init_pos.y = 0.0;
      	  init_pos.z = 0.0;
        }
        else init_pos = _sdf->GetElement("init_pos")->GetValueVector3();

    	////////////// Set up liquid sphere mass
    	if (!_sdf->HasElement("mass"))
    	{
    		std::cout << "Missing parameter <mass> in FactoryLiquid, default to 0 0 0" << std::endl;
    		mass = 0.001;
    	}
    	else mass = _sdf->GetElement("mass")->GetValueDouble();

    	////////////// friction
        if (!_sdf->HasElement("friction"))
        {
      	  std::cout << "Missing parameter <friction> in FactoryLiquid, default to 0" << std::endl;
      	  friction = 0;
        }
        else friction = _sdf->GetElement("friction")->GetValueDouble();

    	////////////// friction2
        if (!_sdf->HasElement("friction2"))
        {
      	  std::cout << "Missing parameter <friction2> in FactoryLiquid, default to 0" << std::endl;
      	  friction2 = 0;
        }
        else friction2 = _sdf->GetElement("friction2")->GetValueDouble();

    	////////////// rolling_friction
        if (!_sdf->HasElement("rolling_friction"))
        {
      	  std::cout << "Missing parameter <rolling_friction> in FactoryLiquid, default to 0" << std::endl;
      	rolling_friction = 0;
        }
        else rolling_friction = _sdf->GetElement("rolling_friction")->GetValueDouble();

    	////////////// cfm
        if (!_sdf->HasElement("cfm"))
        {
      	  std::cout << "Missing parameter <cfm> in FactoryLiquid, default to 0" << std::endl;
      	  cfm = 0;
        }
        else cfm = _sdf->GetElement("cfm")->GetValueDouble();

    	////////////// erp
        if (!_sdf->HasElement("erp"))
        {
      	  std::cout << "Missing parameter <erp> in FactoryLiquid, default to 0" << std::endl;
      	  erp = 0;
        }
        else erp = _sdf->GetElement("erp")->GetValueDouble();

    	////////////// kp
        if (!_sdf->HasElement("kp"))
        {
      	  std::cout << "Missing parameter <kp> in FactoryLiquid, default to 0" << std::endl;
      	kp = 0;
        }
        else kp = _sdf->GetElement("kp")->GetValueDouble();

    	////////////// kd
        if (!_sdf->HasElement("kd"))
        {
      	  std::cout << "Missing parameter <kd> in FactoryLiquid, default to 0" << std::endl;
      	kd = 0;
        }
        else kd = _sdf->GetElement("kd")->GetValueDouble();

    	////////////// restitution_coefficient
    	if (!_sdf->HasElement("restitution_coefficient"))
    	{
    		std::cout << "Missing parameter <restitution_coefficient> in FactoryLiquid, default to 0" << std::endl;
    		restitution_coefficient= 0;
    	}
    	else restitution_coefficient= _sdf->GetElement("restitution_coefficient")->GetValueDouble();

    	////////////// inertia
        if (!_sdf->HasElement("inertia"))
        {
      	  std::cout << "Missing parameter <inertia> in FactoryLiquid, default to 0" << std::endl;
      	inertia = 0;
        }
        else inertia = _sdf->GetElement("inertia")->GetValueDouble();


    	////////////// split_impulse
        if (!_sdf->HasElement("split_impulse"))
        {
      	  std::cout << "Missing parameter <split_impulse> in FactoryLiquid, default to 1 (true)" << std::endl;
      	split_impulse = 1;
        }
        else split_impulse = _sdf->GetElement("split_impulse")->GetValueDouble();

    	////////////// split_impulse_penetration_threshold
        if (!_sdf->HasElement("split_impulse_penetration_threshold"))
        {
      	  std::cout << "Missing parameter <split_impulse_penetration_threshold> in FactoryLiquid, default to -0.01" << std::endl;
      	split_impulse_penetration_threshold = -0.01;
        }
        else split_impulse_penetration_threshold = _sdf->GetElement("split_impulse_penetration_threshold")->GetValueDouble();

        //print parameters
		std::printf("**Liquid parameters:"
				" \n friction: %f"
				" \n friction2: %f"
				" \n rolling_friction: %f"
				" \n cfm: %f"
				" \n erp: %f"
				" \n kp: %f"
				" \n kd: %f"
				" \n restitution_coefficient: %f"
				" \n split_impulse: %f"
				" \n split_impulse_penetration_threshold: %f"
				" \n",
				friction, friction2, rolling_friction, cfm, erp, kp, kd, restitution_coefficient,
				split_impulse, split_impulse_penetration_threshold);

        /////// END SDF PARAMETERS
    	///////////////////////////////////////////////////////////////////////////////////

    	level = 0;
    	spawned = 0;
    	radius = 0.005;

    	p3.x = 0.0;
    	p3.y = 0.0;
    	p3.z = 0.0;


    	///////////////////////////////////////////////////////////////////////////////////
    	///////////////////////////////////////////////////////////////////////////////////
    	//////////////////////////// START XML LIQUID
    	xml << "<?xml version='1.0'?>\n";
    	xml << "<sdf version='1.4'>\n";
    	xml << "<model name='liquid_spheres'>\n";
    	xml << "\t<static>false</static>\n";
    	xml << "\t<pose>" << init_pos.x << " " << init_pos.y << " " << init_pos.z << " 0 0 0 </pose>\n";

    	for (unsigned int i=0; i<nr_spheres; i++)
    	{
    		p3 = FactoryLiquid::part_position(i, radius, spawned, level);
    		xml << "\t\t<link name='sphere_link_" << i << "'>\n";
        	xml << "\t\t\t<self_collide>true</self_collide>\n";
    		xml << "\t\t\t<pose>" << p3.x << " " << p3.y << " " << p3.z << " 0 0 0</pose>\n";

    		xml << "\t\t\t<inertial>\n";
    		xml << "\t\t\t\t<pose> 0 0 0 0 0 0 </pose>\n";
    		xml << "\t\t\t\t<inertia>\n";
    	    xml << "\t\t\t\t\t<ixx>" << inertia << "</ixx>\n";
    	    xml << "\t\t\t\t\t<ixy>0</ixy>\n";
    	    xml << "\t\t\t\t\t<ixz>0</ixz>\n";
    	    xml << "\t\t\t\t\t<iyy>" << inertia << "</iyy>\n";
    	    xml << "\t\t\t\t\t<iyz>0</iyz>\n";
    	    xml << "\t\t\t\t\t<izz>" << inertia << "</izz>\n";
    		xml << "\t\t\t\t</inertia>\n";
    		xml << "\t\t\t\t<mass>" << mass << "</mass>\n";
    		xml << "\t\t\t</inertial>\n";

    		xml << "\t\t\t<collision name='collision_" << i << "'>\n";
    		xml << "\t\t\t\t<geometry>\n";
    		xml << "\t\t\t\t\t<sphere>\n";
    		xml << "\t\t\t\t\t\t<radius>" << radius << "</radius>\n";
    		xml << "\t\t\t\t\t</sphere>\n";
    		xml << "\t\t\t\t</geometry>\n";
    		xml << "\t\t\t\t<surface>\n";
    		xml << "\t\t\t\t\t<friction>\n";
    		xml << "\t\t\t\t\t\t<bullet>\n";
    	    xml << "\t\t\t\t\t\t\t<friction>" << friction << "</friction>\n";
    	    xml << "\t\t\t\t\t\t\t<friction2>" << friction2 << "</friction2>\n";
    	    xml << "\t\t\t\t\t\t\t<fdir1>0.0 0.0 0.0</fdir1>\n";
    	    xml << "\t\t\t\t\t\t\t<rolling_friction>" << rolling_friction << "</rolling_friction>\n";
    		xml << "\t\t\t\t\t\t</bullet>\n";
    		xml << "\t\t\t\t\t</friction>\n";
    		xml << "\t\t\t\t\t<bounce>\n";
    		xml << "\t\t\t\t\t\t<restitution_coefficient>" << restitution_coefficient << "</restitution_coefficient>\n";
    		xml << "\t\t\t\t\t\t<threshold>10000.0</threshold>\n";
    		xml << "\t\t\t\t\t</bounce>\n";
    		xml << "\t\t\t\t\t<contact>\n";
    		xml << "\t\t\t\t\t\t<bullet>\n";
    	    xml << "\t\t\t\t\t\t\t<soft_cfm>" << cfm << "</soft_cfm>\n";
    	    xml << "\t\t\t\t\t\t\t<soft_erp>" << erp << "</soft_erp>\n";
    	    xml << "\t\t\t\t\t\t\t<kp>" << kp << "</kp>\n";
    	    xml << "\t\t\t\t\t\t\t<kd>" << kd << "</kd>\n";
    	    xml << "\t\t\t\t\t\t\t<split_impulse>" << split_impulse << "</split_impulse>\n";
    	    xml << "\t\t\t\t\t\t\t<split_impulse_penetration_threshold>" << split_impulse_penetration_threshold << "</split_impulse_penetration_threshold>\n";
    		xml << "\t\t\t\t\t\t</bullet>\n";
            xml << "\t\t\t\t\t</contact>\n";
    		xml << "\t\t\t\t</surface>\n";
    		xml << "\t\t\t</collision>\n";

    		xml << "\t\t\t<visual name='sphere_visual_" << i << "'>\n";
    		xml << "\t\t\t\t<geometry>\n";
    		xml << "\t\t\t\t\t<sphere>\n";
    		xml << "\t\t\t\t\t\t<radius>" << radius << "</radius>\n";
    		xml << "\t\t\t\t\t</sphere>\n";
    		xml << "\t\t\t\t</geometry>\n";
    		xml << "\t\t\t\t<material>\n";
    		xml << "\t\t\t\t\t<script>\n";
    		xml << "\t\t\t\t\t\t<uri>file://media/materials/scripts/gazebo.material</uri>\n";
    		xml << "\t\t\t\t\t\t<name>Gazebo/Red</name>\n";
    		xml << "\t\t\t\t\t</script>\n";
    		xml << "\t\t\t\t</material>\n";
    		xml << "\t\t\t</visual>\n";
    		xml << "\t\t</link>\n";
    	}

    	/* plugin is a WorldPlugin now, it is loaded in the .world file  */
    	//xml << "<plugin name='dynamic_joint_plugin' filename='libdynamic_joint_plugin.so'/>\n";

		xml << "</model>\n";
		xml << "</gazebo>\n";

		/////////////////////////////////////////
    	//std::cout << xml.str() << "\n";

        sdf::SDF sphereSDF;
        sphereSDF.SetFromString(xml.str());


        _parent->InsertModelSDF(sphereSDF);

    }

    public: math::Vector3 part_position(int i, double radius, int& spawned, int& level)
    {
		math::Vector3 v3;
		int ii, index_c, c_crt, max_c_in_c;
		double size, R;
		ii = i - spawned;
		size = radius * 2;

		v3.z = level * size;
		if (ii != 0)
		{
			index_c = ii-1;
			c_crt = 1;

			while (index_c >= (6*c_crt))
			{
				index_c -= 6*c_crt;
				c_crt++;
			}
			max_c_in_c = c_crt * 6;
			R = c_crt * size;

			if((index_c == (max_c_in_c - 1)) && ((2*R) + (size) >= 0.025))
			{
				spawned = i+1;
				level++;
			}

			v3.x = R * cos((double) index_c * 2 * PI / max_c_in_c);
			v3.y = R * sin((double) index_c * 2 * PI / max_c_in_c);
		}

		return v3;
    }
};

  // Register this plugin with the simulator
  GZ_REGISTER_WORLD_PLUGIN(FactoryLiquid)
}
