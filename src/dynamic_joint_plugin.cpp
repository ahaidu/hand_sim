#include "dynamic_joint_plugin.h"

using namespace gazebo;

DynamicJointPlugin::DynamicJointPlugin()
{
}

DynamicJointPlugin::~DynamicJointPlugin()
{
	delete this->cooking_thread;
	delete this->rosnode;
}

void DynamicJointPlugin::Load(physics::WorldPtr _parent, sdf::ElementPtr _sdf)
    {

	  int argc = 0;
	  char** argv = NULL;
	  ros::init(argc,argv,"Dynamic_joint_plugin");
	  this->rosnode = new ros::NodeHandle();

      ROS_INFO("********DYNAMIC JOINT PLUGIN STARTED*********");

      // Store the pointer to the model
	  this->world = _parent;

	  // set the parameters from the sdf file
	  DynamicJointPlugin::setSDFParameters(_sdf);

	  if (this->type.compare("hydra") == 0)
	  {
		  //std::cout << "hydra shall control the creation of the pancake" << std::endl;
		  this->ros_subscriber = this->rosnode->subscribe("/hydra_raw", 10, &DynamicJointPlugin::HydraDynamicJointCallback, this);
	  }
	  else if (this->type.compare("threegear") == 0)
	  {
		  //std::cout << "threegear shall control the creation of the pancake" << std::endl;
		  this->ros_subscriber = this->rosnode->subscribe("/threegear_gesture", 10, &DynamicJointPlugin::ThreegearDynamicJointCallback, this);
	  }

      DynamicJointPlugin::initModels();
    }

void DynamicJointPlugin::OnUpdate()
    {
    }

void DynamicJointPlugin::HydraDynamicJointCallback(const hand_sim::HydraRaw& msg)
{
	if (msg.buttons[1] == 1)
	{
		this->jointButtonStatus = true;
	}

	if(this->jointButtonStatus && msg.buttons[1] == 0)
	{
		if (!this->jointCreated)
		{
			// get the liquid model
			this->liquid_model = this->world->GetModel("liquid_spheres");

			// start the cooking thread function
			//this->cooking_thread = new boost::thread(boost::bind(
			//		&DynamicJointPlugin::CookingPancakeThreadFunc, this));
			DynamicJointPlugin::createPancake();

			this->jointCreated = true;
		}
		this->jointButtonStatus = false;
	}
}

void DynamicJointPlugin::ThreegearDynamicJointCallback(const hand_sim::ThreegearGesture& msg)
{
	/*  1 PRESSED  /  2 DRAGGED  /  3 RELEASED */
	if (msg.gesture_left == 2)
	{
		this->jointButtonStatus = true;
	}

	if(this->jointButtonStatus && msg.gesture_left == 3)
	{
		if (!this->jointCreated)
		{
			// get the liquid model
			this->liquid_model = this->world->GetModel("liquid_spheres");
			DynamicJointPlugin::createPancake();

			this->jointCreated = true;
		}
		this->jointButtonStatus = false;
	}
}

void DynamicJointPlugin::initModels()
{
    this->jointButtonStatus = false;
    this->jointCreated = false;
}

void DynamicJointPlugin::CookingPancakeThreadFunc()
{
	const common::Time curr_sim_time = this->world->GetSimTime();
	const common::Time sleep = 0.01;
	const common::Time cook_time = 3;
	const std::vector<physics::LinkPtr> links = this->liquid_model->GetLinks();
	int step = 0;

	std::cout << " Cooking :) " << std::endl;

	std::cout << this->world->GetSimTime() - curr_sim_time << std::endl;

/*	while (this->world->GetSimTime() - curr_sim_time < cook_time )
	{
		step = step + 0.1;

		for (unsigned int i = 0; i < links.size(); ++i)
		{
			// Changing the pancakes (external spheres) surface parameters, making its surface harder
			links[i]->GetCollision(0)->GetSurface()->kp = step;
			links[i]->GetCollision(0)->GetSurface()->kd = step;
		}

		common::Time::Sleep(sleep);
	}*/

	for (unsigned int i = 0; i < links.size(); ++i)
	{
		// Changing the pancakes (external spheres) surface parameters, making its surface harder
		links[i]->GetCollision(0)->GetSurface()->kp = 1;
		links[i]->GetCollision(0)->GetSurface()->kd = 1;
	}

	common::Time::Sleep(cook_time);
	std::cout << " Done cooking :) " << std::endl;

	// Create the joints of the pancake
	DynamicJointPlugin::createPancake();

}

void DynamicJointPlugin::createPancake()
{
	pcl::PointCloud<pcl::PointXYZ> cloud;
	pcl::PointXYZ center_pos;
	unsigned int center_index;
	std::stringstream ss;

	// Change the surface parameters of the spheres
	const std::vector<physics::LinkPtr> links = this->liquid_model->GetLinks();
	for (unsigned int i = 0; i < links.size(); ++i)
	{
		links[i]->GetCollision(0)->GetSurface()->kp = 10000000;
		links[i]->GetCollision(0)->GetSurface()->kd = 100;
	}
	std::cout << " Pancake surface hardened " << std::endl;

	// create point cloud with the position of the spheres
	DynamicJointPlugin::initLiquidCloud(cloud);

	// input cloud, output center pos
	DynamicJointPlugin::getCloudCenter(cloud, center_pos);

	// get center link index
	center_index = DynamicJointPlugin::GetClosestPoint(cloud, center_pos);

	// set center_link
	ss << "sphere_link_" << center_index;
	this->center_link = this->liquid_model->GetLink(ss.str());
	ss.str("");

	// Changing the pancakes spheres inertial values
	this->center_link->GetInertial()->SetInertiaMatrix(this->pancake_inertia , this->pancake_inertia, this->pancake_inertia,0,0,0);
	//Testing purpose, for joint axis
/*	this->center_link->GetInertial()->SetInertiaMatrix(0.001 , 0.001, 0.001, 0,0,0);
	this->center_link->GetInertial()->SetMass(100);*/
	this->center_link->UpdateMass();


	// create joints between center link and the others
	for (unsigned int i=0; i < cloud.size(); i++)
	{
		if (i != center_index)
		{
			ss << "sphere_link_" << i;

			// Changing the pancakes spheres inertial values
			this->liquid_model->GetLink(ss.str())->GetInertial()->SetInertiaMatrix(this->pancake_inertia,this->pancake_inertia,this->pancake_inertia,0,0,0);
			this->liquid_model->GetLink(ss.str())->UpdateMass();

			DynamicJointPlugin::createJoint(this->center_link, this->liquid_model->GetLink(ss.str()));
			ss.str("");
		}
	}

	ROS_WARN("PANCAKE CREATED!");

}


void DynamicJointPlugin::createJoint(physics::LinkPtr _center_link, physics::LinkPtr _ext_link)
{
	math::Vector3 axis,direction;

	direction = _ext_link->GetWorldPose().pos - _center_link->GetWorldPose().pos;
	direction.Normalize();
	axis = direction.Cross(math::Vector3(0.0, 0.0, 0.1));

	//ROS_INFO("**CREATING JOINT** between links \"%s\" and \"%s\"",_center_link->GetName().c_str(), _ext_link->GetName().c_str());

	this->myJoints.push_back(this->liquid_model->GetWorld()->GetPhysicsEngine()->CreateJoint("revolute", this->liquid_model));

	this->myJoints.back()->Attach(_ext_link, _center_link );

	this->myJoints.back()->Load(_ext_link, _center_link , math::Pose(_center_link->GetWorldPose().pos, math::Quaternion()));

	this->myJoints.back()->SetAxis(0, axis);

	// reducing the error reductions parameter allows joint to exceed the limit
	this->myJoints.back()->SetAttribute("stop_cfm",0, this->stop_cfm);
	this->myJoints.back()->SetAttribute("stop_erp",0, this->stop_erp);
	this->myJoints.back()->SetAttribute("hi_stop",0, this->limit_stop);
	this->myJoints.back()->SetAttribute("lo_stop",0, - this->limit_stop);
	this->myJoints.back()->SetAttribute("fudge_factor", 0, this->fudge_factor);
	this->myJoints.back()->SetAttribute("cfm", 0, this->cfm);
	this->myJoints.back()->SetAttribute("erp", 0, this->erp);

}

void DynamicJointPlugin::initLiquidCloud(pcl::PointCloud<pcl::PointXYZ> &cloud)
{
	std::stringstream ss;
	unsigned int i = 0;
	ss << "sphere_link_" << i;


	while (this->liquid_model->GetLink(ss.str()) != NULL)
	{
		//ROS_INFO("link %u",i);
		math::Vector3 pos = this->liquid_model->GetLink(ss.str())->GetWorldPose().pos;
		cloud.push_back(pcl::PointXYZ(pos.x, pos.y, pos.z));
		i++;
		ss.str("");
		ss << "sphere_link_" << i;
	}



}

void DynamicJointPlugin::getCloudCenter(pcl::PointCloud<pcl::PointXYZ> &cloud, pcl::PointXYZ &center_pos)
{
	Eigen::Vector4f centroid;
	pcl::compute3DCentroid(cloud, centroid);

	center_pos.x = centroid[0];
	center_pos.y = centroid[1];
	center_pos.z = centroid[2];

	ROS_INFO("center X: %f",center_pos.x);

}

unsigned int DynamicJointPlugin::GetClosestPoint(pcl::PointCloud<pcl::PointXYZ> &cloud, pcl::PointXYZ pos)
{
  pcl::KdTree<pcl::PointXYZ>::Ptr tree;

  cloud.push_back(pos);

  tree = boost::make_shared<pcl::KdTreeFLANN<pcl::PointXYZ> > ();
  tree->setInputCloud(cloud.makeShared());

  int k = 2;
  std::vector<int> k_indices;
  std::vector<float> k_distances;
  k_indices.resize(k);
  k_distances.resize(k);

  //THE CENTER IS THE NEAREST NEIGHBOUR OF THE ADDED POINT, WHICH IS DELETED AFTERWARDS
  //last point of the cloud (the center)
  int index = (int)cloud.points.size()-1;
  tree->nearestKSearch(index, k, k_indices, k_distances);

  cloud.points.pop_back();

  //ROS_INFO("center indice: %u",k_indices[1]);

  return k_indices[1];
}


void DynamicJointPlugin::setSDFParameters(const sdf::ElementPtr _sdf)
{
	  ////////////// type
	  if (!_sdf->HasElement("type"))
	  {
		  std::cout << "Missing parameter <type> in DynamicJoint, default to hydra" << std::endl;
		  this->type = "hydra";
	  }
	  else this->type = _sdf->GetElement("type")->GetValueString();

	  ////////////// stop_cfm
	  if (!_sdf->HasElement("stop_cfm"))
	  {
		  std::cout << "Missing parameter <stop_cfm> in DynamicJoint, default to 0" << std::endl;
		  this->stop_cfm = 0;
	  }
	  else this->stop_cfm = _sdf->GetElement("stop_cfm")->GetValueDouble();

	  ////////////// stop_erp
	  if (!_sdf->HasElement("stop_erp"))
	  {
		  std::cout << "Missing parameter <stop_erp> in DynamicJoint, default to 0" << std::endl;
		  this->stop_erp = 0;
	  }
	  else this->stop_erp = _sdf->GetElement("stop_erp")->GetValueDouble();

	  ////////////// fudge_factor
	  if (!_sdf->HasElement("fudge_factor"))
	  {
		  std::cout << "Missing parameter <fudge_factor> in DynamicJoint, default to 0" << std::endl;
		  this->fudge_factor = 0;
	  }
	  else this->fudge_factor = _sdf->GetElement("fudge_factor")->GetValueDouble();

	  ////////////// cfm
	  if (!_sdf->HasElement("cfm"))
	  {
		  std::cout << "Missing parameter <cfm> in DynamicJoint, default to 0" << std::endl;
		  this->cfm = 0;
	  }
	  else this->cfm = _sdf->GetElement("cfm")->GetValueDouble();

	  ////////////// erp
	  if (!_sdf->HasElement("erp"))
	  {
		  std::cout << "Missing parameter <erp> in DynamicJoint, default to 0" << std::endl;
		  this->erp = 0;
	  }
	  else this->erp = _sdf->GetElement("erp")->GetValueDouble();


	  ////////////// hi / lo _stop
	  if (!_sdf->HasElement("limit_stop"))
	  {
		  std::cout << "Missing parameter <limit_stop> in DynamicJoint, default to 0" << std::endl;
		  this->limit_stop = 0;
	  }
	  else this->limit_stop = _sdf->GetElement("limit_stop")->GetValueDouble();

	  ////////////// pancake_inertia
	  if (!_sdf->HasElement("limit_stop"))
	  {
		  std::cout << "Missing parameter <pancake_inertia> in DynamicJoint, default to 0.00002" << std::endl;
		  this->pancake_inertia = 0.00002;
	  }
	  else this->pancake_inertia = _sdf->GetElement("pancake_inertia")->GetValueDouble();

}

// Register this plugin with the simulator
  GZ_REGISTER_WORLD_PLUGIN(DynamicJointPlugin)


