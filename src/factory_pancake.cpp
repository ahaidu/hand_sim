#include "gazebo.hh"
#include "physics/physics.hh"
#include "common/Plugin.hh"
#include "transport/transport.hh"
#include <math.h>

#define PI 3.14159265

namespace gazebo
{
  class FactoryPancake : public WorldPlugin
  {
	public: virtual ~FactoryPancake()
	{

	}

    public: void Load(physics::WorldPtr _parent, sdf::ElementPtr _sdf)
    {
    	std::stringstream xml;

    	sdf::Vector3 init_pos;

    	double sphere_radius, pancake_radius, mass, limitStop, mu, mu2, slip1, slip2, c_cfm, c_erp, kp, kd,
    	 	 	 fudge_factor, j_cfm, bounce, limit_cfm, limit_erp, inertia;

    	///////////////////////////////////////////////////////////////////////////////////
    	/////// SDF PARAMETERS

    	////////////// Set up the initial position parameter
    	if (!_sdf->HasElement("init_pos"))
    	{
    		std::cout << "Missing parameter <init_pos> in FactoryPancake, default to 0 0 0" << std::endl;
    		init_pos.x = 0.0;
    		init_pos.y = 0.0;
    		init_pos.z = 0.0;
    	}
    	else init_pos = _sdf->GetElement("init_pos")->GetValueVector3();

    	////////////// Set up pancake_radius
    	if (!_sdf->HasElement("pancake_radius"))
    	{
    		std::cout << "Missing parameter <pancake_radius> in FactoryPancake, default to 0.0344" << std::endl;
    		pancake_radius = 0.0344;
    	}
    	else pancake_radius = _sdf->GetElement("pancake_radius")->GetValueDouble();

    	////////////// Set up the sphere_radius parameter
    	if (!_sdf->HasElement("sphere_radius"))
    	{
    		std::cout << "Missing parameter <sphere_radius> in FactoryPancake, default to 0.007" << std::endl;
    		sphere_radius = 0.007;
    	}
    	else sphere_radius = _sdf->GetElement("sphere_radius")->GetValueDouble();

    	////////////// Set up pancake sphere mass
    	if (!_sdf->HasElement("mass"))
    	{
    		std::cout << "Missing parameter <mass> in FactoryPancake, default to 0.001" << std::endl;
    		mass = 0.001;
    	}
    	else mass = _sdf->GetElement("mass")->GetValueDouble();

    	////////////// Set up pancake joint angle +/- limits
    	if (!_sdf->HasElement("limitStop"))
    	{
    		std::cout << "Missing parameter <limitStop> in FactoryPancake, default to 0" << std::endl;
    		limitStop = 0.0;
    	}
    	else limitStop = _sdf->GetElement("limitStop")->GetValueDouble();

    	////////////// mu
    	if (!_sdf->HasElement("mu"))
    	{
    		std::cout << "Missing parameter <mu> in FactoryPancake, default to 0" << std::endl;
    		mu = 0;
    	}
    	else mu = _sdf->GetElement("mu")->GetValueDouble();

    	////////////// mu2
    	if (!_sdf->HasElement("mu2"))
    	{
    		std::cout << "Missing parameter <mu2> in FactoryPancake, default to 0" << std::endl;
    		mu2 = 0;
    	}
    	else mu2 = _sdf->GetElement("mu2")->GetValueDouble();

    	////////////// slip1
    	if (!_sdf->HasElement("slip1"))
    	{
    		std::cout << "Missing parameter <slip1> in FactoryPancake, default to 0" << std::endl;
    		slip1 = 0;
    	}
    	else slip1 = _sdf->GetElement("slip1")->GetValueDouble();

    	////////////// slip2
    	if (!_sdf->HasElement("slip2"))
    	{
    		std::cout << "Missing parameter <slip2> in FactoryPancake, default to 0" << std::endl;
    		slip2 = 0;
    	}
    	else slip2 = _sdf->GetElement("slip2")->GetValueDouble();

    	////////////// CONTACT JOINTS
    	//////////////  cfm
    	if (!_sdf->HasElement("c_cfm"))
    	{
    		std::cout << "Missing parameter <c_cfm> in FactoryPancake, default to 0" << std::endl;
    		c_cfm = 0;
    	}
    	else c_cfm = _sdf->GetElement("c_cfm")->GetValueDouble();

    	////////////// erp
    	if (!_sdf->HasElement("c_erp"))
    	{
    		std::cout << "Missing parameter <c_erp> in FactoryPancake, default to 0" << std::endl;
    		c_erp = 0;
    	}
    	else c_erp = _sdf->GetElement("c_erp")->GetValueDouble();

    	////////////// kp
    	if (!_sdf->HasElement("kp"))
    	{
    		std::cout << "Missing parameter <kp> in FactoryPancake, default to 0" << std::endl;
    		kp = 0;
    	}
    	else kp = _sdf->GetElement("kp")->GetValueDouble();

    	////////////// kd
    	if (!_sdf->HasElement("kd"))
    	{
    		std::cout << "Missing parameter <kd> in FactoryPancake, default to 0" << std::endl;
    		kd = 0;
    	}
    	else kd = _sdf->GetElement("kd")->GetValueDouble();

    	////////////// REVOLUTE JOINTS
    	//////////////  fudge_factor
    	if (!_sdf->HasElement("fudge_factor"))
    	{
    		std::cout << "Missing parameter <fudge_factor> in FactoryPancake, default to 0" << std::endl;
    		fudge_factor = 0;
    	}
    	else fudge_factor = _sdf->GetElement("fudge_factor")->GetValueDouble();

    	////////////// cfm
    	if (!_sdf->HasElement("j_cfm"))
    	{
    		std::cout << "Missing parameter <j_cfm> in FactoryPancake, default to 0" << std::endl;
    		j_cfm = 0;
    	}
    	else j_cfm = _sdf->GetElement("j_cfm")->GetValueDouble();

    	////////////// bounce
    	if (!_sdf->HasElement("bounce"))
    	{
    		std::cout << "Missing parameter <bounce> in FactoryPancake, default to 0" << std::endl;
    		bounce = 0;
    	}
    	else bounce = _sdf->GetElement("bounce")->GetValueDouble();

    	////////////// limit_cfm
    	if (!_sdf->HasElement("limit_cfm"))
    	{
    		std::cout << "Missing parameter <limit_cfm> in FactoryPancake, default to 0" << std::endl;
    		limit_cfm = 0;
    	}
    	else limit_cfm = _sdf->GetElement("limit_cfm")->GetValueDouble();

    	////////////// limit_erp
    	if (!_sdf->HasElement("limit_erp"))
    	{
    		std::cout << "Missing parameter <limit_erp> in FactoryPancake, default to 0" << std::endl;
    		limit_erp = 0;
    	}
    	else limit_erp = _sdf->GetElement("limit_erp")->GetValueDouble();

    	////////////// inertia
    	if (!_sdf->HasElement("inertia"))
    	{
    		std::cout << "Missing parameter <inertia> in FactoryPancake, default to 0" << std::endl;
    		inertia = 0;
    	}
    	else inertia = _sdf->GetElement("inertia")->GetValueDouble();


    	//print parameters
    	std::printf("**Pancake parameters:"
    			" \n mu: %f"
    			" \n mu2: %f"
    			" \n slip1: %f"
    			" \n slip2: %f"
    			" \n c_cfm: %f"
    			" \n c_erp: %f"
    			" \n kp: %f"
    			" \n kd: %f"
    			" \n fudge_factor: %f"
    			" \n j_cfm: %f"
    			" \n bounce: %f"
    			" \n limit_cfm: %f"
    			" \n limit_erp: %f \n",
    			mu, mu2, slip1, slip2, c_cfm, c_erp, kp, kd,
    			fudge_factor, j_cfm, bounce, limit_cfm, limit_erp);

    	/////// END SDF PARAMETERS
    	///////////////////////////////////////////////////////////////////////////////////



    	unsigned int n_min = 7; //smallest number of spheres on the smallest inner circle
    	unsigned int circles = int(PI * pancake_radius/sphere_radius);
    	unsigned int n_circles = 0;
    	double x, y, x_axis, y_axis /*, limitStop*/;

    	///////////////////////////////////////////////////////////////////////////////////
    	///////////////////////////////////////////////////////////////////////////////////
    	//////////////////////////// START XML PANCAKE ////////////////////////////////////

    	xml << "<sdf version ='1.4'>\n";
    	xml << "<model name ='pancake'>\n";
    	xml << "\t<static>false</static>\n";
    	xml << "\t<pose>" << init_pos.x << " " << init_pos.y << " " << init_pos.z << " 0 0 0 </pose>\n";

    	xml << "\t\t<link name='sphere_link_center'>\n";
    	xml << "\t\t\t<self_collide>true</self_collide>\n";
    	xml << "\t\t\t<pose>0 0 0 0 0 0</pose>\n";
    	xml << "\t\t\t<inertial>\n";
    	xml << "\t\t\t\t<pose>0 0 0 0 0 0</pose>\n";
    	xml << "\t\t\t\t<inertia>\n";
	    xml << "\t\t\t\t\t<ixx>" << inertia << "</ixx>\n";
	    xml << "\t\t\t\t\t<ixy>0</ixy>\n";
	    xml << "\t\t\t\t\t<ixz>0</ixz>\n";
	    xml << "\t\t\t\t\t<iyy>" << inertia << "</iyy>\n";
	    xml << "\t\t\t\t\t<iyz>0</iyz>\n";
	    xml << "\t\t\t\t\t<izz>" << inertia << "</izz>\n";
    	xml << "\t\t\t\t</inertia>\n";
    	xml << "\t\t\t\t<mass>" << mass*5 << "</mass>\n";
    	xml << "\t\t\t</inertial>\n";

    	xml << "\t\t\t<collision name ='collision_center'>\n";
    	xml << "\t\t\t\t<geometry>\n";
    	xml << "\t\t\t\t\t<sphere>\n";
    	xml << "\t\t\t\t\t\t<radius>" << sphere_radius << "</radius>\n";
    	xml << "\t\t\t\t\t</sphere>\n";
    	xml << "\t\t\t\t</geometry>\n";
    	xml << "\t\t\t\t<surface>\n";
    	xml << "\t\t\t\t\t<friction>\n";
    	xml << "\t\t\t\t\t\t<ode>\n";
    	xml << "\t\t\t\t\t\t\t<mu>" << mu << "</mu>\n";
    	xml << "\t\t\t\t\t\t\t<mu2>" << mu2 << "</mu2>\n";
    	xml << "\t\t\t\t\t\t\t<fdir1>0.0 0.0 0.0</fdir1>\n";
    	xml << "\t\t\t\t\t\t\t<slip1>" << slip1 << "</slip1>\n";
    	xml << "\t\t\t\t\t\t\t<slip2>" << slip2 << "</slip2>\n";
    	xml << "\t\t\t\t\t\t</ode>\n";
    	xml << "\t\t\t\t\t</friction>\n";
    	xml << "\t\t\t\t\t<bounce>\n";
    	xml << "\t\t\t\t\t\t<restitution_coefficient>" << bounce << "</restitution_coefficient>\n";
    	xml << "\t\t\t\t\t\t<threshold>10000.0</threshold>\n";
    	xml << "\t\t\t\t\t</bounce>\n";
    	xml << "\t\t\t\t\t<contact>\n";
    	xml << "\t\t\t\t\t\t<ode>\n";
    	xml << "\t\t\t\t\t\t\t<soft_cfm>" << c_cfm << "</soft_cfm>\n";
    	xml << "\t\t\t\t\t\t\t<soft_erp>" << c_erp << "</soft_erp>\n";
    	xml << "\t\t\t\t\t\t\t<kp>" << kp << "</kp>\n";
    	xml << "\t\t\t\t\t\t\t<kd>" << kd << "</kd>\n";
    	xml << "\t\t\t\t\t\t\t<max_vel>100.0</max_vel>\n";
    	xml << "\t\t\t\t\t\t\t<min_depth>0.001</min_depth>\n";
    	xml << "\t\t\t\t\t\t</ode>\n";
    	xml << "\t\t\t\t\t</contact>\n";
    	xml << "\t\t\t\t</surface>\n";
    	xml << "\t\t\t</collision>\n";

    	xml << "\t\t\t<visual name ='visual_center'>\n";
		xml << "\t\t\t\t<geometry>\n";
		xml << "\t\t\t\t\t<sphere>\n";
		xml << "\t\t\t\t\t\t<radius>" << sphere_radius << "</radius>\n";
		xml << "\t\t\t\t\t</sphere>\n";
		xml << "\t\t\t\t</geometry>\n";
		xml << "\t\t\t\t<material>\n";
		xml << "\t\t\t\t\t<script>\n";
		xml << "\t\t\t\t\t\t<uri>file://media/materials/scripts/gazebo.material</uri>\n";
		xml << "\t\t\t\t\t\t<name>Gazebo/Red</name>\n";
		xml << "\t\t\t\t\t</script>\n";
		xml << "\t\t\t\t</material>\n";
		xml << "\t\t\t</visual>\n";
    	xml << "\t\t</link>\n";

    	///////////////////////////////////////////////////////////////////
    	while (circles > n_min)
    	{
    		circles = circles/2;
    		n_circles++;
    		//ROS_ERROR("n_circles: %i", n_circles);
    	}

    	//limitStop = (double) 50/(n_circles*2)/100;
    	///////////////////////////////////////////////////////////////////
    	////LINKS
    	for (int i=0; i < (int) PI*(pancake_radius*2/pow(2,n_circles))/sphere_radius; i++)
    	{
    		x = (pancake_radius*2/pow(2,n_circles)) * cos(i * 2 * PI / (PI * (pancake_radius*2/pow(2,n_circles))/sphere_radius));
    		y = (pancake_radius*2/pow(2,n_circles)) * sin(i * 2 * PI / (PI * (pancake_radius*2/pow(2,n_circles))/sphere_radius));

    		x_axis = (double) -y / sqrt((x*x)+(y*y));
    		if (x_axis == -0){x_axis = 0;}
    		y_axis = (double)  x / sqrt((x*x)+(y*y));

    		xml << "\t\t<link name ='sphere_link_" << n_circles << "_" << i << "'>\n";
        	xml << "\t\t\t<self_collide>true</self_collide>\n";
        	xml << "\t\t\t<pose>" << x << " " << y << " 0 0 0 0</pose>\n";
        	xml << "\t\t\t<inertial>\n";
        	xml << "\t\t\t\t<pose> 0 0 0 0 0 0 </pose>\n";
        	xml << "\t\t\t\t<inertia>\n";
        	xml << "\t\t\t\t\t<ixx>0.000001</ixx>\n";
        	xml << "\t\t\t\t\t<ixy>0</ixy>\n";
        	xml << "\t\t\t\t\t<ixz>0</ixz>\n";
        	xml << "\t\t\t\t\t<iyy>0.000001</iyy>\n";
        	xml << "\t\t\t\t\t<iyz>0</iyz>\n";
        	xml << "\t\t\t\t\t<izz>0.000001</izz>\n";
        	xml << "\t\t\t\t</inertia>\n";
        	xml << "\t\t\t\t<mass>" << mass << "</mass>\n";
        	xml << "\t\t\t</inertial>\n";

    		xml << "\t\t\t<collision name ='collision_" << n_circles << "_" << i << "'>\n";
    		xml << "\t\t\t\t<geometry>\n";
    		xml << "\t\t\t\t\t<sphere>\n";
    		xml << "\t\t\t\t\t\t<radius>" << sphere_radius << "</radius>\n";
    		xml << "\t\t\t\t\t</sphere>\n";
    		xml << "\t\t\t\t</geometry>\n";
    		xml << "\t\t\t\t<surface>\n";
    		xml << "\t\t\t\t\t<friction>\n";
    		xml << "\t\t\t\t\t\t<ode>\n";
    	    xml << "\t\t\t\t\t\t\t<mu>" << mu << "</mu>\n";
    	    xml << "\t\t\t\t\t\t\t<mu2>" << mu2 << "</mu2>\n";
    	    xml << "\t\t\t\t\t\t\t<fdir1>0.0 0.0 0.0</fdir1>\n";
    	    xml << "\t\t\t\t\t\t\t<slip1>" << slip1 << "</slip1>\n";
    	    xml << "\t\t\t\t\t\t\t<slip2>" << slip2 << "</slip2>\n";
    		xml << "\t\t\t\t\t\t</ode>\n";
    		xml << "\t\t\t\t\t</friction>\n";
    		xml << "\t\t\t\t\t<bounce>\n";
    		xml << "\t\t\t\t\t\t<restitution_coefficient>0</restitution_coefficient>\n";
    		xml << "\t\t\t\t\t\t<threshold>10000.0</threshold>\n";
    		xml << "\t\t\t\t\t</bounce>\n";
    		xml << "\t\t\t\t\t<contact>\n";
    		xml << "\t\t\t\t\t\t<ode>\n";
    	    xml << "\t\t\t\t\t\t\t<soft_cfm>" << c_cfm << "</soft_cfm>\n";
    	    xml << "\t\t\t\t\t\t\t<soft_erp>" << c_erp << "</soft_erp>\n";
    	    xml << "\t\t\t\t\t\t\t<kp>" << kp << "</kp>\n";
    	    xml << "\t\t\t\t\t\t\t<kd>" << kd << "</kd>\n";
    	    xml << "\t\t\t\t\t\t\t<max_vel>100.0</max_vel>\n";
    	    xml << "\t\t\t\t\t\t\t<min_depth>0.001</min_depth>\n";
    		xml << "\t\t\t\t\t\t</ode>\n";
            xml << "\t\t\t\t\t</contact>\n";
    		xml << "\t\t\t\t</surface>\n";
    		xml << "\t\t\t</collision>\n";

    		xml << "\t\t\t<visual name ='visual_" << n_circles << "_" << i << "'>\n";
    		xml << "\t\t\t\t<geometry>\n";
    		xml << "\t\t\t\t\t<sphere>\n";
    		xml << "\t\t\t\t\t\t<radius>" << sphere_radius << "</radius>\n";
    		xml << "\t\t\t\t\t</sphere>\n";
    		xml << "\t\t\t\t</geometry>\n";
    		xml << "\t\t\t\t<material>\n";
    		xml << "\t\t\t\t\t<script>\n";
    		xml << "\t\t\t\t\t\t<uri>file://media/materials/scripts/gazebo.material</uri>\n";
    		xml << "\t\t\t\t\t\t<name>Gazebo/Red</name>\n";
    		xml << "\t\t\t\t\t</script>\n";
    		xml << "\t\t\t\t</material>\n";
    		xml << "\t\t\t</visual>\n";
    		xml << "\t\t</link>\n";

    		xml << "\t\t<joint name ='joint_" << n_circles << "_" << i << "' type='revolute'>\n";
    		xml << "\t\t\t\t<pose>0 0 0 0 0 0</pose>\n";
    		xml << "\t\t\t\t<parent>sphere_link_" << n_circles << "_" << i << "</parent>\n";
    		xml << "\t\t\t\t<child>sphere_link_center</child>\n";
    		/*xml << "\t\t\t\t<parent>sphere_link_center</parent>\n";
    		xml << "\t\t\t\t<child>sphere_link_" << n_circles << "_" << i << "</child>\n";*/
    		xml << "\t\t\t\t<axis>\n";
    		xml << "\t\t\t\t\t<dynamics>\n";
    		xml << "\t\t\t\t\t\t<damping>0.0</damping>\n";
    		xml << "\t\t\t\t\t</dynamics>\n";
    		xml << "\t\t\t\t\t<limit>\n";
    		xml << "\t\t\t\t\t\t<lower>" << -limitStop << "</lower>\n";
    		xml << "\t\t\t\t\t\t<upper>" << limitStop << "</upper>\n";
    		xml << "\t\t\t\t\t\t<effort>0.0</effort>\n";
    		xml << "\t\t\t\t\t\t<velocity>0.0</velocity>\n";
    		xml << "\t\t\t\t\t</limit>\n";
    		xml << "\t\t\t\t\t<xyz>" << x_axis << " " << y_axis << " 0</xyz>\n";
    		xml << "\t\t\t\t</axis>\n";

    		xml << "\t\t\t\t<physics>\n";
    		xml << "\t\t\t\t\t<ode>\n";
    	    xml << "\t\t\t\t\t\t<fudge_factor>" << fudge_factor << "</fudge_factor>\n";
    	    xml << "\t\t\t\t\t\t<cfm>" << j_cfm << "</cfm>\n";
    	    xml << "\t\t\t\t\t\t<limit>\n";
    	    xml << "\t\t\t\t\t\t\t<cfm>" << limit_cfm << "</cfm>\n";
    	    xml << "\t\t\t\t\t\t\t<erp>" << limit_erp << "</erp>\n";
    	    xml << "\t\t\t\t\t\t</limit>\n";
    		xml << "\t\t\t\t\t</ode>\n";
    		xml << "\t\t\t\t</physics>\n";
    		xml << "\t\t</joint>\n";
    	}


    	/*for (int j = n_circles-1; j > 0; j--)
    	{
    		//ROS_ERROR("j: %i",j);
    		for (int i=0; i < (int) PI*(pancake_radius*2/pow(2,j))/sphere_radius; i++)
    		{
    			//ROS_ERROR("i: %i",i);
    			x = (pancake_radius*2/pow(2,j)) * cos(i * 2 * PI / (PI * (pancake_radius*2/pow(2,j))/sphere_radius));
    			y = (pancake_radius*2/pow(2,j)) * sin(i * 2 * PI / (PI * (pancake_radius*2/pow(2,j))/sphere_radius));

    			x_axis = (double) -y / sqrt((x*x)+(y*y));
    			if (x_axis == -0){x_axis = 0;}
    			y_axis = (double)  x / sqrt((x*x)+(y*y));

    			xml << "\t\t<link name ='sphere_link_" << j << "_" << i << "'>\n";
            	xml << "\t\t\t<self_collide>true</self_collide>\n";
            	xml << "\t\t\t<pose>" << x << " " << y << " 0 0 0 0</pose>\n";
            	xml << "\t\t\t<inertial>\n";
            	xml << "\t\t\t\t<pose> 0 0 0 0 0 0 </pose>\n";
            	xml << "\t\t\t\t<inertia>\n";
            	xml << "\t\t\t\t\t<ixx>" << inertia << "</ixx>\n";
            	xml << "\t\t\t\t\t<ixy>0</ixy>\n";
            	xml << "\t\t\t\t\t<ixz>0</ixz>\n";
            	xml << "\t\t\t\t\t<iyy>" << inertia << "</iyy>\n";
            	xml << "\t\t\t\t\t<iyz>0</iyz>\n";
            	xml << "\t\t\t\t\t<izz>" << inertia << "</izz>\n";
            	xml << "\t\t\t\t</inertia>\n";
            	xml << "\t\t\t\t<mass>" << mass << "</mass>\n";
            	xml << "\t\t\t</inertial>\n";

    			xml << "\t\t\t<collision name ='collision_" << j << "_" << i << "'>\n";
        		xml << "\t\t\t\t<geometry>\n";
        		xml << "\t\t\t\t\t<sphere>\n";
        		xml << "\t\t\t\t\t\t<radius>" << sphere_radius << "</radius>\n";
        		xml << "\t\t\t\t\t</sphere>\n";
        		xml << "\t\t\t\t</geometry>\n";
        		xml << "\t\t\t\t<surface>\n";
        		xml << "\t\t\t\t\t<friction>\n";
        		xml << "\t\t\t\t\t\t<ode>\n";
        	    xml << "\t\t\t\t\t\t\t<mu>" << mu << "</mu>\n";
        	    xml << "\t\t\t\t\t\t\t<mu2>" << mu2 << "</mu2>\n";
        	    xml << "\t\t\t\t\t\t\t<fdir1>0.0 0.0 0.0</fdir1>\n";
        	    xml << "\t\t\t\t\t\t\t<slip1>" << slip1 << "</slip1>\n";
        	    xml << "\t\t\t\t\t\t\t<slip2>" << slip2 << "</slip2>\n";
        		xml << "\t\t\t\t\t\t</ode>\n";
        		xml << "\t\t\t\t\t</friction>\n";
        		xml << "\t\t\t\t\t<bounce>\n";
        		xml << "\t\t\t\t\t\t<restitution_coefficient>" << bounce << "</restitution_coefficient>\n";
        		xml << "\t\t\t\t\t\t<threshold>10000.0</threshold>\n";
        		xml << "\t\t\t\t\t</bounce>\n";
        		xml << "\t\t\t\t\t<contact>\n";
        		xml << "\t\t\t\t\t\t<ode>\n";
        	    xml << "\t\t\t\t\t\t\t<soft_cfm>" << c_cfm << "</soft_cfm>\n";
        	    xml << "\t\t\t\t\t\t\t<soft_erp>" << c_erp << "</soft_erp>\n";
        	    xml << "\t\t\t\t\t\t\t<kp>" << kp << "</kp>\n";
        	    xml << "\t\t\t\t\t\t\t<kd>" << kd << "</kd>\n";
        	    xml << "\t\t\t\t\t\t\t<max_vel>100.0</max_vel>\n";
        	    xml << "\t\t\t\t\t\t\t<min_depth>0.001</min_depth>\n";
        		xml << "\t\t\t\t\t\t</ode>\n";
                xml << "\t\t\t\t\t</contact>\n";
        		xml << "\t\t\t\t</surface>\n";
        		xml << "\t\t\t</collision>\n";

    			xml << "\t\t\t<visual name ='visual_" << j << "_" << i << "'>\n";
        		xml << "\t\t\t\t<geometry>\n";
        		xml << "\t\t\t\t\t<sphere>\n";
        		xml << "\t\t\t\t\t\t<radius>" << sphere_radius << "</radius>\n";
        		xml << "\t\t\t\t\t</sphere>\n";
        		xml << "\t\t\t\t</geometry>\n";
        		xml << "\t\t\t\t<material>\n";
        		xml << "\t\t\t\t\t<script>\n";
        		xml << "\t\t\t\t\t\t<uri>file://media/materials/scripts/gazebo.material</uri>\n";
        		xml << "\t\t\t\t\t\t<name>Gazebo/Red</name>\n";
        		xml << "\t\t\t\t\t</script>\n";
        		xml << "\t\t\t\t</material>\n";
        		xml << "\t\t\t</visual>\n";
        		xml << "\t\t</link>\n";

    			xml << "\t\t<joint name ='joint_" << n_circles << "_" << i << "' type='revolute'>\n";
        		xml << "\t\t\t\t<pose>0 0 0 0 0 0</pose>\n";
        		xml << "\t\t\t\t<parent>sphere_link_center</parent>\n";
        		xml << "\t\t\t\t<child>sphere_link_" << n_circles << "_" << i << "</child>\n";
        		xml << "\t\t\t\t<axis>\n";
        		xml << "\t\t\t\t\t<dynamics>\n";
        		xml << "\t\t\t\t\t\t<damping>0.005</damping>\n";
        		xml << "\t\t\t\t\t</dynamics>\n";
        		xml << "\t\t\t\t\t<limit>\n";
        		xml << "\t\t\t\t\t\t<lower>" << -limitStop << "</lower>\n";
        		xml << "\t\t\t\t\t\t<upper>" << limitStop << "</upper>\n";
        		xml << "\t\t\t\t\t\t<effort>0.0</effort>\n";
        		xml << "\t\t\t\t\t\t<velocity>0.0</velocity>\n";
        		xml << "\t\t\t\t\t</limit>\n";
        		xml << "\t\t\t\t\t<xyz>" << x_axis << " " << y_axis << " 0</xyz>\n";
        		xml << "\t\t\t\t</axis>\n";

        		xml << "\t\t\t\t<physics>\n";
        		xml << "\t\t\t\t\t<ode>\n";
        	    xml << "\t\t\t\t\t\t<fudge_factor>" << fudge_factor << "</fudge_factor>\n";
        	    xml << "\t\t\t\t\t\t<cfm>" << j_cfm << "</cfm>\n";
        	    xml << "\t\t\t\t\t\t<limit>\n";
        	    xml << "\t\t\t\t\t\t\t<cfm>" << limit_cfm << "</cfm>\n";
        	    xml << "\t\t\t\t\t\t\t<erp>" << limit_erp << "</erp>\n";
        	    xml << "\t\t\t\t\t\t</limit>\n";
        		xml << "\t\t\t\t\t</ode>\n";
        		xml << "\t\t\t\t</physics>\n";
        		xml << "\t\t</joint>\n";
    		}
    	}*/

    	///////////////////////////////////////////////////////////////////
    	xml << "</model>\n";
    	xml << "</gazebo>\n";

    	/////////////////////////////////
    	//std::cout << xml.str() << "\n";

    	sdf::SDF pancakeSDF;
    	pancakeSDF.SetFromString(xml.str());

    	_parent->InsertModelSDF(pancakeSDF);

    }

  };

  // Register this plugin with the simulator
  GZ_REGISTER_WORLD_PLUGIN(FactoryPancake)
}
