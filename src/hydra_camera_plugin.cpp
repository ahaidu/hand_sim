#include "hydra_camera_plugin.h"

using namespace gazebo;
using namespace rendering;

// Register this plugin with the simulator
//GZ_REGISTER_WORLD_PLUGIN(HydraCameraPlugin)
GZ_REGISTER_SYSTEM_PLUGIN(HydraCameraPlugin)

HydraCameraPlugin::HydraCameraPlugin()
{
	int argc = 0;
	char** argv = NULL;
	ros::init(argc,argv,"Hydra_plugin");
	this->rosnode = new ros::NodeHandle();

	this->hydra_subscriber = this->rosnode->subscribe("/hydra_raw", 100, &HydraCameraPlugin::HydraCallback, this);
}

HydraCameraPlugin::~HydraCameraPlugin()
{
}

void HydraCameraPlugin::Load(int /*_argc*/, char ** /*_argv*/)
{
	std::cout << "********** Hydra Camera Plugin Started ********" << std::endl;



    // Listen to the update event. This event is broadcast every simulation iteration.
    // this->updateConnection = event::Events::ConnectWorldUpdateBegin(boost::bind(&HydraCameraPlugin::OnUpdate, this));
}

void HydraCameraPlugin::Init()
{
	std::cout << "********** In INIT ********" << std::endl;

	this->activeCam = gui::get_active_camera();

	this->activeCam->GetScene();

	// Create a visual that is used a reference point.
	this->refVisual.reset(new Visual("HydraViewController",
			this->activeCam->GetScene()));

	this->refVisual->Init();
	this->refVisual->AttachMesh("unit_sphere");
	this->refVisual->SetScale(math::Vector3(0.2, 0.2, 0.1));
	this->refVisual->SetCastShadows(false);
	this->refVisual->SetMaterial("Gazebo/YellowTransparent");
	this->refVisual->SetVisible(false);
	this->refVisual->SetVisibilityFlags(GZ_VISIBILITY_GUI);

	this->focalPoint = math::Vector3(0,0,0);

	this->reference_pos_set = false;
	this->flags_set = false;


	// Create spinner thread in order to run the callbacks
	this->spinner_thread = new boost::thread(boost::bind(&HydraCameraPlugin::spinnerFunc, this));

	this->activeCam->EnableViewController(false);

	std::cout << "********** Out of INIT ********" << std::endl;


}
void HydraCameraPlugin::HydraCallback(const hand_sim::HydraRaw& msg)
{
	//std::cout << "********** Hydra Callback ********" << std::endl;

	if (msg.analog[5] == 0)
	{
		if (!this->flags_set)
		{
			this->reference_pos_set = false;
			this->refVisual->SetVisible(false);
			std::cout << "*** Enable View Controller " << std::endl;
			this->activeCam->EnableViewController(true);
			this->flags_set = true;
		}
		return;
	}

	if (!this->reference_pos_set)
	{
		math::Vector3 dir = this->activeCam->GetDirection();
		dir.Normalize();
		this->refCameraPos = this->activeCam->GetWorldPosition();
		this->focalPoint = this->refCameraPos + dir * 3;
		this->refVisual->SetPosition(this->focalPoint);
		this->refVisual->SetVisible(true);
		this->reference_pos_set = true;
		std::cout << "*** Disable View Controller " << std::endl;
		this->activeCam->EnableViewController(false);
		this->flags_set = false;
	}

	math::Vector3 translation;

// Left controller sensor is trolling using the one from the right at the moment
//	translation.Set(this->focalPoint.x - (float) msg.pos[4] / 200,
//			this->focalPoint.y - (float) msg.pos[3] / 200,
//			this->focalPoint.z - (float) msg.pos[5] / 200);


	translation.Set(this->refCameraPos.x - (float) msg.pos[1] / 200,
			this->refCameraPos.y - (float) msg.pos[0] / 200,
			this->refCameraPos.z - (float) msg.pos[2] / 200);

	this->activeCam->Update();
	this->activeCam->SetWorldPosition(translation);



//	std::cout << this->focalPoint.x - (float) msg.pos[4] / 200 << std::endl;
//	std::cout << this->activeCam->GetWorldPosition() + translation << std::endl;

}

void HydraCameraPlugin::spinnerFunc()
{
	std::cout << "********** In SpinnerFunc ********" << std::endl;
	//ros::spinOnce();

	ros::spin();
	std::cout << "********** Out SpinnerFunc ********" << std::endl;
}












