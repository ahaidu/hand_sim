#include "hydra_plugin.h"
#include <boost/tokenizer.hpp>
#include "gazebo/util/LogRecord.hh"

#define PI 3.14159265359

using namespace gazebo;

HydraPlugin::HydraPlugin()
{
	int argc = 0;
	char** argv = NULL;
	ros::init(argc,argv,"Hydra_plugin");
	this->rosnode = new ros::NodeHandle();

	this->hydra_subscriber = this->rosnode->subscribe(
			"/hydra_raw", 100, &HydraPlugin::HydraCallback, this);

}

HydraPlugin::~HydraPlugin()
{
	delete this->rosnode;
	this->contactNode->Fini();
}

void HydraPlugin::Load(physics::ModelPtr _parent, sdf::ElementPtr _sdf)
{
	// Store the pointer to the model
	this->hand_model = _parent;

	// Listen to the update event. This event is broadcast every simulation iteration.
	this->updateConnection = event::Events::ConnectWorldUpdateBegin(
			boost::bind(&HydraPlugin::OnUpdate, this));

	//disable gravity on the hand model
	this->hand_model->SetGravityMode(false);

	//get current time of the simulation
	this->time_of_last_cycle = this->hand_model->GetWorld()->GetSimTime();

	this->pid_controller_x.Reset();
	this->pid_controller_y.Reset();
	this->pid_controller_z.Reset();

	/*initialize translation PID*/
	HydraPlugin::InitPid(17, 0, 10.0, 10, -10);

	/*set initial desired position of the hand (the spawned position)*/
	this->curr_position = this->hand_model->GetWorldPose().pos;
	this->desired_position.x = this->curr_position.x;
	this->desired_position.y = this->curr_position.y;
	this->desired_position.z = this->curr_position.z;

	/*set the initial value of the rotation*/
	this->desired_quatern.Set(0.0, 0.0, 0.0, 0.0);

	// get the sdf parameters from the plugin
	HydraPlugin::GetSdfParameters(_sdf);

	// initialize Hydra variables
	HydraPlugin::HydraInit();

	// create listener to the contact topic, and callback OnContact
	this->contactNode = transport::NodePtr(new transport::Node());

	this->contactNode->Init();

	this->contactSub = this->contactNode->Subscribe("~/thumb_contact", &HydraPlugin::OnContact, this);

	this->world = this->hand_model->GetWorld();

	this->fixed_joint = this->world->GetPhysicsEngine()->CreateJoint(
			"revolute", this->hand_model);

	// initialize Gazebo transport node
	this->node = transport::NodePtr(new transport::Node());

	// Initialize the node with the world name
	this->node->Init(this->hand_model->GetWorld()->GetName());

	// set advertising topic
	this->worldControlPub =
			this->node->Advertise<msgs::WorldControl>("~/world_control");


	ROS_INFO("********HYDRA PLUGIN STARTED*********");
}

// Called by the world update start event
void HydraPlugin::OnUpdate()
{
	// spinOnce is also used for calling the other plugin callbacks (e.g. mug, spatula)
	ros::spinOnce();

	/* current pose of the model*/
	this->curr_pose = this->hand_model->GetWorldPose();
	this->curr_quatern = curr_pose.rot;
	this->curr_position = curr_pose.pos;

	/*delta time between cycles*/
	common::Time dt = (double) this->hand_model->GetWorld()->GetSimTime().Double() - this->time_of_last_cycle.Double();
	this->time_of_last_cycle = this->hand_model->GetWorld()->GetSimTime();

	/*computing lin/rot velocities*/
	this->pid_lin_velocity.x = pid_controller_x.Update(this->curr_position.x - this->desired_position.x, dt);
	this->pid_lin_velocity.y = pid_controller_y.Update(this->curr_position.y - this->desired_position.y, dt);
	this->pid_lin_velocity.z = pid_controller_z.Update(this->curr_position.z - this->desired_position.z, dt);

	this->rot_velocity = HydraPlugin::ReturnRotVelocity();

	/*apply the computed lin/rot forces/velocities to the model*/
	//this->hand_model->GetLinkById(0)->SetForce(this->pid_lin_velocity);
	this->hand_model->GetLink("right_hand_palm")->SetForce(this->pid_lin_velocity);
	this->hand_model->SetAngularVel(this->rot_velocity);

}


void HydraPlugin::InitPid(double P, double I, double D, double I1_max,  double I2_min)
{
	  this->pid_controller_x.Init(P, I, D, I1_max, I2_min);
	  this->pid_controller_y.Init(P, I, D, I1_max, I2_min);
	  this->pid_controller_z.Init(P, I, D, I1_max, I2_min);
}

void HydraPlugin::HydraInit()
{
	//offset so the hand points forwards
	//this->hydra_offset_q.Set(0.129540, -0.831947, -0.056893, 0.633260);
	this->hydra_offset_q = math::Quaternion(0,PI/2,PI);

	this->offset_position.x = 400 / precision;
	this->offset_position.y = 0;
	this->offset_position.z = 1;

	this->hydra_on = false;
	this->hydraOnOffButtonState = false;
	this->hydraPauseButtonState = false;

	this->gripper_button_pressed = false;
	this->fixed_joint_attached = false;

	this->curr_logging = false;


	// Gripper mode
	this->hand_model->GetJoint("right_hand_thumb_joint")->SetAngle(0, 1.57);
    // Set hand joints
	HydraPlugin::setHandJoints();

}

math::Vector3 HydraPlugin::ReturnRotVelocity()
{
    math::Vector3 rot_velocity;
    math::Quaternion quatern_diff, vel_q;

    quatern_diff = (this->desired_quatern - curr_quatern)*2.0;
    vel_q = quatern_diff * this->curr_quatern.GetInverse();

    rot_velocity.x = vel_q.x*2;
    rot_velocity.y = vel_q.y*2;
    rot_velocity.z = vel_q.z*2;

    return rot_velocity;
}

void HydraPlugin::HydraCallback(const hand_sim::HydraRaw& msg)
{

	if (msg.buttons[1] == 32)
	{
		this->hydraOnOffButtonState = true;//pressed
	}
	if(this->hydraOnOffButtonState && msg.buttons[1] == 0)
	{
		this->hydra_on = !this->hydra_on;
		this->hydraOnOffButtonState = false;//not pressed
		if(hydra_on)
		{
			// Start logging after we have the control of the hand
			HydraPlugin::StartLogging();
		}
	}

	/* Pause/Play the world*/
	if (msg.buttons[1] == 2)
	{
		this->hydraPauseButtonState = true;//pressed
	}
	if(this->hydraPauseButtonState && msg.buttons[1] == 0)
	{
		this->hydraPauseButtonState = false;//not pressed
		/* after pausing the world spin() is unable to be called, so unpause is done from the GUI*/
		HydraPlugin::PauseWorld();
	}

	if(hydra_on)
	{
		HydraPlugin::ControlPose(msg);

		///////////////////////////////////////////////////////
		//Gripper mode
 	 	if (msg.analog[5] > 0)
		{
 	 		this->gripper_button_pressed = true;
 	 	 	// Close / Open gripper
 	 		if (!this->fixed_joint_attached)
 	 		{
 	 			// Close gripper
 	 			HydraPlugin::closeGripper(true);
 	 		}
 	 		else
 	 		{
 	 			// Keep fingers in position
 	 			HydraPlugin::closeGripper(false);
 	 		}

		}
		else
		{
 	 		this->gripper_button_pressed = false;
 	 	 	HydraPlugin::openGripper();
		}
		///////////////////////////////////////////////////////

	}

}

void HydraPlugin::ControlPose(const hand_sim::HydraRaw& msg)
{
	  math::Quaternion q_msg, q_msg_offseted;
	  q_msg.Set( - (float)msg.quat[0]/1000, (float)msg.quat[2]/1000, (float)msg.quat[1]/1000, (float)msg.quat[3]/1000);
	  q_msg_offseted = q_msg * this->hydra_offset_q;

	  this->desired_position.x = - (float) msg.pos[1] / this->precision + this->offset_position.x;
	  this->desired_position.y = - (float) msg.pos[0] / this->precision + this->offset_position.y;
	  this->desired_position.z = - (float) msg.pos[2] / this->precision + this->offset_position.z;

	  this->desired_quatern = this->hydra_slide_offset_q * q_msg_offseted;
	  //this->desired_quatern.Normalize();

}

//////////////////////////////////////////////////////////////////////////////////////
void HydraPlugin::setHandJoints()
{
    // set up thumb joints
    this->hand_joints.push_back(this->hand_model->GetJoint("right_hand_thumb_proximal_joint"));
    this->hand_joints.push_back(this->hand_model->GetJoint("right_hand_thumb_middle_joint"));
    this->hand_joints.push_back(this->hand_model->GetJoint("right_hand_thumb_distal_joint"));
}

//////////////////////////////////////////////////////////////////////////////////////
void HydraPlugin::closeGripper(bool close_flag)
{
	if(close_flag)
	{
		const double force = 6000;
		// only thumb
		this->hand_joints[0]->SetForce(0, force);
		this->hand_joints[1]->SetForce(0, force/4);
		this->hand_joints[2]->SetForce(0, force/8);
	}
}

//////////////////////////////////////////////////////////////////////////////////////
void HydraPlugin::openGripper()
{
	if (this->hand_joints[0]->GetAngle(0).Radian() > 0.1)
	{
		const double force = 6000;
		// only thumb
		this->hand_joints[0]->SetForce(0, - force/2);
		this->hand_joints[1]->SetForce(0, - force);
		this->hand_joints[2]->SetForce(0, - force);
	}
}

//////////////////////////////////////////////////////////////////////////////////////
void HydraPlugin::OnContact(ConstContactsPtr &_msg)
{
	if (this->gripper_button_pressed && _msg->contact_size() > 0 && !this->fixed_joint_attached)
	{
		// open gripper before attaching the joint so there is no actual contact
		// the object acts weird if there is an actual contact
		HydraPlugin::openGripper();

		//std::cout << "collision1: " << _msg->contact(_msg->contact_size()-1).collision1().c_str() << std::endl;
		//std::cout << "collision2: " << _msg->contact(_msg->contact_size()-1).collision2().c_str() << std::endl;

		boost::char_separator<char> sep("::");

		boost::tokenizer< boost::char_separator<char> > tok(_msg->contact(0).collision1(), sep);
		boost::tokenizer< boost::char_separator<char> >::iterator beg=tok.begin();

		physics::ModelPtr attachModel = this->world->GetModel(*beg);
		//std::cout << "attach Model: " << attachModel->GetName().c_str() << std::endl;
		beg++;
		physics::LinkPtr attachLink = attachModel->GetLink(*beg);
		//std::cout << "attach Link: " << attachLink->GetName().c_str() << std::endl;

		HydraPlugin::attachJoint(attachLink);
	}
	else if(!this->gripper_button_pressed && this->fixed_joint_attached)
	{
		HydraPlugin::detachJoint();

	}
}

//////////////////////////////////////////////////////////////////////////////////////
void HydraPlugin::attachJoint(const physics::LinkPtr attachLink)
{
	physics::LinkPtr palm_link = this->hand_model->GetLink("right_hand_palm");

	std::cout << "Creating joint between " << palm_link->GetName().c_str()
			<< " and " << attachLink->GetName().c_str() << std::endl;

	this->fixed_joint->Load(palm_link, attachLink, math::Pose());

    this->fixed_joint->Init();
    this->fixed_joint->SetHighStop(0, 0);
    this->fixed_joint->SetLowStop(0, 0);

	this->fixed_joint_attached = true;

	// Start logging data (if the first joint it's with the mug)
	// HydraPlugin::StartLogging();

	// Log action timestamp
	std::stringstream ss;
	ss << " Creating joint with: " << attachLink->GetName();
	HydraPlugin::LogTimestamp( ss.str() );
}

//////////////////////////////////////////////////////////////////////////////////////
void HydraPlugin::detachJoint()
{
	// Log action timestamp
	std::stringstream ss;
	ss << " Detaching joint from: " << this->fixed_joint->GetChild()->GetName();
	HydraPlugin::LogTimestamp( ss.str() );

	// Stop logging the data (if the detached joint is with the spatula)
	if (!std::strcmp(this->fixed_joint->GetChild()->GetName().c_str(), "spatula_link"))
	{
		HydraPlugin::StopLogging();
	}

	std::cout << "Detaching Fixed Joint! " <<  std::endl;

	//this->fixed_joint->Reset();
	this->fixed_joint->Detach();
	this->fixed_joint->Fini();

	this->fixed_joint_attached = false;
}

void HydraPlugin::LogTimestamp(std::string msg)
{
	  std::ofstream myfile;
	  myfile.open ("/home/haidu/.gazebo/log/ActionLog.txt", std::ios::out | std::ios::app);
	  //myfile << msg.c_str()<< " sec " << this->mug_model->GetWorld()->GetSimTime().sec << " nsec " << this->mug_model->GetWorld()->GetSimTime().nsec << "\n";
	  myfile << this->world->GetSimTime() << " " << msg.c_str() << "\n";
	  myfile.close();
}

void HydraPlugin::GetSdfParameters(const sdf::ElementPtr &_sdf)
{
	// get parameters from sdf file
	////////////// precision
	if (!_sdf->HasElement("precision"))
	{
		std::cout << "Missing parameter <precision> in FactoryLiquid, default to 200" << std::endl;
		this->precision = 200;
	}
	else this->precision = _sdf->GetElement("precision")->GetValueDouble();


	//TODO see why we cannot get bool values, now its used as int
	//this->logging_on = true;
	////////////// loggin_on flag
	if (!_sdf->HasElement("logging_on"))
	{
		std::cout << "Missing parameter <logging_on> in FactoryLiquid, default to false ( 0 )" << std::endl;
		this->logging_on = 0;
	}
	else this->logging_on = (int) _sdf->GetElement("logging_on")->GetValueDouble();
	if (logging_on == 1)
	{
		std::cout << "Logging ON" << std::endl;
	}
	else
	{
		std::cout << "Logging OFF" << std::endl;
	}
}

void HydraPlugin::StartLogging()
{
	if ((this->logging_on == 1) && (!this->curr_logging))
	{
		std::cout << "Start logging" << std::endl;
		gazebo::util::LogRecord::Instance()->Start("zlib"); //  [bz2, zlib, txt]
		this->curr_logging = true;
	}
}

void HydraPlugin::StopLogging()
{
	// Stop logging the data (if the detached joint is with the spatula)
	if ((this->logging_on == 1) && (this->curr_logging))
	{
		std::cout << "Stop logging, " << this->fixed_joint->GetChild()->GetName().c_str() << " released" << std::endl;
		gazebo::util::LogRecord::Instance()->Stop();
		this->curr_logging = false;
	}
}

//////////////////////////////////////////////////
void HydraPlugin::PauseWorld()
{
	/* after pausing the world spin() is unable to be called, so unpause is done from the GUI*/
    // Create and send a msg with pausing the world when the plugin is initialized
    msgs::WorldControl msg;
    msg.set_pause(true);
    this->worldControlPub->Publish(msg);
}

// Register this plugin with the simulator
  GZ_REGISTER_MODEL_PLUGIN(HydraPlugin)


