#include "hydra_pose_plugin.h"

#define PI 3.14159265359

using namespace gazebo;

//////////////////////////////////////////////////
HydraPosePlugin::HydraPosePlugin()
{
	int argc = 0;
	char** argv = NULL;
	ros::init(argc,argv,"hydra_pose_plugin");
	this->rosnode = new ros::NodeHandle();

	this->hydra_subscriber = this->rosnode->subscribe(
			"/hydra_raw", 1, &HydraPosePlugin::HydraCallback, this);


	this->hydra_on = false;
	this->hydraOnOffButtonState = false;
	this->offsetPositionSet = false;
}

//////////////////////////////////////////////////
HydraPosePlugin::~HydraPosePlugin()
{
	delete this->rosnode;
}

//////////////////////////////////////////////////
void HydraPosePlugin::Load(physics::ModelPtr _parent, sdf::ElementPtr _sdf)
{
	// Store the pointer to the model
	this->myModel = _parent;

	// Listen to the update event. This event is broadcast every simulation iteration.
	this->updateConnection = event::Events::ConnectWorldUpdateBegin(
			boost::bind(&HydraPosePlugin::OnUpdate, this));

	//disable gravity on the hand model
	this->myModel->SetGravityMode(false);

	//get current time of the simulation
	this->time_of_last_cycle = this->myModel->GetWorld()->GetSimTime();

	this->pid_controller_x.Reset();
	this->pid_controller_y.Reset();
	this->pid_controller_z.Reset();

	// Init Pid controllers
	this->pid_controller_x.Init(7, 0, 10, 10, -10);
	this->pid_controller_y.Init(7, 0, 10, 10, -10);
	this->pid_controller_z.Init(7, 0, 10, 10, -10);

	/*set initial desired position of the hand (the spawned position)*/
	this->curr_position = this->myModel->GetWorldPose().pos;
	this->desired_position.x = this->curr_position.x;
	this->desired_position.y = this->curr_position.y;
	this->desired_position.z = this->curr_position.z;

	/*set the initial value of the rotation*/
	this->desired_quatern.Set(0.0, 0.0, 0.0, 0.0);

	//offset so the hand points forwards
	this->hydra_offset_q = math::Quaternion(0,PI/2,PI);


	ROS_INFO("********HYDRA MODEL POSE CONTROLLER PLUGIN STARTED*********");
}

//////////////////////////////////////////////////
void HydraPosePlugin::OnUpdate()
{
	ros::spinOnce();

	/* current pose of the model*/
	this->curr_pose = this->myModel->GetWorldPose();
	this->curr_quatern = curr_pose.rot;
	this->curr_position = curr_pose.pos;

	/*delta time between cycles*/
	common::Time dt = (double) this->myModel->GetWorld()->GetSimTime().Double() - this->time_of_last_cycle.Double();
	this->time_of_last_cycle = this->myModel->GetWorld()->GetSimTime();

	/*computing lin/rot velocities*/
	this->pid_lin_velocity.x = pid_controller_x.Update(this->curr_position.x - this->desired_position.x, dt);
	this->pid_lin_velocity.y = pid_controller_y.Update(this->curr_position.y - this->desired_position.y, dt);
	this->pid_lin_velocity.z = pid_controller_z.Update(this->curr_position.z - this->desired_position.z, dt);

	this->rot_velocity = HydraPosePlugin::ReturnRotVelocity();

	/*apply the computed lin/rot forces/velocities to the model*/
	//this->hand_model->GetLinkById(0)->SetForce(this->pid_lin_velocity);
	this->myModel->GetLinks().front()->SetForce(this->pid_lin_velocity);
	this->myModel->SetAngularVel(this->rot_velocity);
}

//////////////////////////////////////////////////
void HydraPosePlugin::HydraCallback(const hand_sim::HydraRaw& msg)
{
	if (msg.buttons[1] == 32)
	{
		this->hydraOnOffButtonState = true;//pressed
	}

	if(this->hydraOnOffButtonState && msg.buttons[1] == 0)
	{
		this->hydra_on = !this->hydra_on;
		this->hydraOnOffButtonState = false;//not pressed

		if (!this->offsetPositionSet)
		{
			this->offsetPosition = math::Vector3(- (float) msg.pos[1] / 1000,
												- (float) msg.pos[0] / 1000,
												- (float) msg.pos[2] / 1000);
			this->offsetPositionSet = true;
		}
	}

	if(hydra_on)
	{
		  math::Quaternion q_msg, q_msg_offseted;
		  q_msg.Set( - (float)msg.quat[0]/1000,
					  (float)msg.quat[2]/1000,
					  (float)msg.quat[1]/1000,
					  (float)msg.quat[3]/1000);
		  q_msg_offseted = q_msg * this->hydra_offset_q;

		  this->desired_position.x = - ((float) msg.pos[1] / 1000) - this->offsetPosition.x + 0.2;
		  this->desired_position.y = - ((float) msg.pos[0] / 1000) - this->offsetPosition.y - 0.15;
		  this->desired_position.z = - ((float) msg.pos[2] / 1000) - this->offsetPosition.z + 0.9;

		  this->desired_quatern = q_msg_offseted;

	}
}


math::Vector3 HydraPosePlugin::ReturnRotVelocity()
{
    math::Vector3 rot_velocity;
    math::Quaternion quatern_diff, vel_q;

    quatern_diff = (this->desired_quatern - curr_quatern)*2.0;
    vel_q = quatern_diff * this->curr_quatern.GetInverse();

    rot_velocity.x = vel_q.x*2;
    rot_velocity.y = vel_q.y*2;
    rot_velocity.z = vel_q.z*2;

    return rot_velocity;
}


// Register this plugin with the simulator
  GZ_REGISTER_MODEL_PLUGIN(HydraPosePlugin)
