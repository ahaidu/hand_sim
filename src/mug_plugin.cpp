#include "mug_plugin.h"

using namespace gazebo;

MugPlugin::MugPlugin()
{
	  int argc = 0;
	  char** argv = NULL;
	  ros::init(argc,argv,"Mug_plugin");
	  this->rosnode = new ros::NodeHandle();

	  this->hydra_subscriber = this->rosnode->subscribe("/hydra_raw", 10, &MugPlugin::MugCallback, this);
}

MugPlugin::~MugPlugin()
{
}

void MugPlugin::Load(physics::ModelPtr _parent, sdf::ElementPtr /*_sdf*/)
    {
      // Store the pointer to the model
      this->mug_model = _parent;

      // Listen to the update event. This event is broadcast every simulation iteration.
      //this->updateConnection = event::Events::ConnectWorldUpdateBegin(boost::bind(&MugPlugin::OnUpdate, this));

      MugPlugin::initModels();

      ROS_INFO("********MUG PLUGIN STARTED*********");
   }

    // Called by the world update start event
void MugPlugin::OnUpdate()
    {
		//ROS_INFO("Mug PLUGIN RUNNING!!");
    }

void MugPlugin::MugCallback(const hand_sim::HydraRaw& msg)
{
	if (msg.buttons[1] == 8)
	{
		this->hydraJointButtonStatus = true;
	}

	if(this->hydraJointButtonStatus && msg.buttons[1] == 0)
	{
		if (!this->jointCreated)
		{
			this->mug_model->SetGravityMode(false);
			MugPlugin::createJoint();
			this->jointCreated = true;
		}
		else
		{
			MugPlugin::destroyJoint();
			this->mug_model->SetGravityMode(true);
			this->jointCreated = false;
		}
		this->hydraJointButtonStatus = false;
	}
}

void MugPlugin::initModels()
{
    this->hydraJointButtonStatus = false;
    this->jointCreated = false;

    if (this->mug_model->GetWorld()->GetEntity("hit_hand") != NULL)
    {
    	// init rosie_model if we have the hit hand in the world
    	this->rosie_model = this->mug_model->GetWorld()->GetEntity("hit_hand")->GetParentModel();
    }
}

void MugPlugin::createJoint()
{
	// Log Timestamp to file
	MugPlugin::logTimestamp("creating joint with Mug at:");

	ROS_INFO("**CREATING JOINT** between links \"%s\" and \"%s\"",this->mug_model->GetLink("mug_link")->GetName().c_str(), this->rosie_model->GetLink("right_hand_palm")->GetName().c_str());

	this->myJoint = this->mug_model->GetWorld()->GetPhysicsEngine()->CreateJoint("revolute", this->mug_model);

	this->myJoint->Attach(this->mug_model->GetLink("mug_link"),this->rosie_model->GetLink("right_hand_palm"));

	this->myJoint->SetAttribute("hi_stop",0, 0.0);
	this->myJoint->SetAttribute("lo_stop",0,-0.0);
}

void MugPlugin::destroyJoint()
{
	// Log Timestamp to file
	MugPlugin::logTimestamp("destroying joint with Mug at:");

	ROS_WARN("**DELETING JOINT** between links \"%s\" and \"%s\"",this->mug_model->GetLink("mug_link")->GetName().c_str(), this->rosie_model->GetLink("right_hand_palm")->GetName().c_str());
	this->myJoint->Detach();
}

void MugPlugin::logTimestamp(std::string msg)
{
	  std::ofstream myfile;
	  myfile.open ("/home/haidu/.gazebo/log/joint_logs.txt", std::ios::out | std::ios::app);
	  myfile << msg.c_str()<< " sec " << this->mug_model->GetWorld()->GetSimTime().sec << " nsec " << this->mug_model->GetWorld()->GetSimTime().nsec << "\n";
	  myfile.close();
}

// Register this plugin with the simulator
  GZ_REGISTER_MODEL_PLUGIN(MugPlugin)


