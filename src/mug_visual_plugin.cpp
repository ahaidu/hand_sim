#include "mug_visual_plugin.h"
#include <OGRE/Ogre.h>

using namespace gazebo;

GZ_REGISTER_VISUAL_PLUGIN(MugVisualPlugin)

void MugVisualPlugin::Load(rendering::VisualPtr _parent, sdf::ElementPtr /*_sdf*/)
{
	this->link_visual = _parent;

	this->link_visual->SetVisible(false, false);

	this->updateConnection = event::Events::ConnectPreRender(boost::bind(&MugVisualPlugin::OnUpdate, this));

    std::cout << ("********MUG VISUAL PLUGIN STARTED*********") << std::endl;
}

void MugVisualPlugin::OnUpdate()
{

	//hardcoded at the moment
	double distance = math::Vector2d(this->link_visual->GetWorldPose().pos.x, this->link_visual->GetWorldPose().pos.y).Distance(math::Vector2d(0.3, 0.5));

	MugVisualPlugin::DrawLine(this->link_visual->GetWorldPose().pos,
							  math::Vector3(0.3, 0.5, 0.9),
							  "help_line",
							  this->link_visual->GetScene()->GetManager(),
							  distance);

	//this->link_visual->GetScene()->DrawLine(this->link_visual->GetWorldPose().pos, math::Vector3(0.3, 0.5, 0.9), "help_line");


/*	//hardcoded at the moment
	double distance = math::Vector2d(this->link_visual->GetWorldPose().pos.x, this->link_visual->GetWorldPose().pos.y).Distance(math::Vector2d(0.3, 0.5));

	if (distance > 0.1)
	{
		this->link_visual->SetVisible(false, false);
	}
	else
	{
		this->link_visual->SetVisible(true, false);
		common::Color c(0.0, 1 - (distance * 10), 0.0);
	    this->link_visual->SetAmbient(c);
	    this->link_visual->SetDiffuse(c);
	}*/

}

//////////////////////////////////////////////////
void MugVisualPlugin::DrawLine(const math::Vector3 &start_,
                     const math::Vector3 &end_,
                     const std::string &name_,
                     Ogre::SceneManager *manager_,
                     const double &distance_)
{
  Ogre::SceneNode *sceneNode = NULL;
  Ogre::ManualObject *obj = NULL;
  bool attached = false;

  if (manager_->hasManualObject(name_))
  {
    sceneNode = manager_->getSceneNode(name_);
    obj = manager_->getManualObject(name_);
    attached = true;
  }
  else
  {
    sceneNode = manager_->getRootSceneNode()->createChildSceneNode(name_);
    obj = manager_->createManualObject(name_);
  }

  sceneNode->setVisible(true);
  obj->setVisible(true);

  obj->clear();
  if (distance_ < 0.05)
  {
	  obj->begin("Gazebo/Green",Ogre::RenderOperation::OT_LINE_LIST);
  }
  else if (distance_ < 0.1)
  {
	  obj->begin("Gazebo/Yellow",Ogre::RenderOperation::OT_LINE_LIST);
  }
  else
  {
	  obj->begin("Gazebo/Red",Ogre::RenderOperation::OT_LINE_LIST);
  }
  obj->colour(0.0, 1 - (distance_ * 10), 0.0, 1.0);
  obj->position(start_.x, start_.y, start_.z);
  obj->position(end_.x, end_.y, end_.z);
  obj->end();

  if (!attached)
    sceneNode->attachObject(obj);
}
