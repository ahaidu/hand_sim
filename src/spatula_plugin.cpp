#include "spatula_plugin.h"

using namespace gazebo;

SpatulaPlugin::SpatulaPlugin()
{
	int argc = 0;
	char** argv = NULL;
	ros::init(argc,argv,"Spatula_plugin");
	this->rosnode = new ros::NodeHandle();

	this->hydra_subscriber = this->rosnode->subscribe("/hydra_raw", 10, &SpatulaPlugin::SpatulaCallback, this);
}

SpatulaPlugin::~SpatulaPlugin()
{
}

void SpatulaPlugin::Load(physics::ModelPtr _parent, sdf::ElementPtr _sdf)
{
	// Store the pointer to the model
	this->spatula_model = _parent;

	// Listen to the update event. This event is broadcast every simulation iteration.
	//this->updateConnection = event::Events::ConnectWorldUpdateBegin(boost::bind(&SpatulaPlugin::OnUpdate, this));

	SpatulaPlugin::initModels();

	// get parameters from sdf file

	////////////// stop_cfm
	if (!_sdf->HasElement("stop_cfm"))
	{
		std::cout << "Missing parameter <stop_cfm> in FactoryLiquid, default to 0" << std::endl;
		this->stop_cfm = 0;
	}
	else this->stop_cfm = _sdf->GetElement("stop_cfm")->GetValueDouble();

	////////////// stop_erp
	if (!_sdf->HasElement("stop_erp"))
	{
		std::cout << "Missing parameter <stop_erp> in FactoryLiquid, default to 0" << std::endl;
		this->stop_erp = 0;
	}
	else this->stop_erp = _sdf->GetElement("stop_erp")->GetValueDouble();

	////////////// hi_stop
	if (!_sdf->HasElement("hi_stop"))
	{
		std::cout << "Missing parameter <hi_stop> in FactoryLiquid, default to 0" << std::endl;
		this->hi_stop = 0;
	}
	else this->hi_stop = _sdf->GetElement("hi_stop")->GetValueDouble();

	////////////// lo_stop
	if (!_sdf->HasElement("lo_stop"))
	{
		std::cout << "Missing parameter <lo_stop> in FactoryLiquid, default to 0" << std::endl;
		this->lo_stop = 0;
	}
	else this->lo_stop = _sdf->GetElement("lo_stop")->GetValueDouble();


	ROS_INFO("********SPATULA PLUGIN STARTED*********");
}

// Called by the world update start event
void SpatulaPlugin::OnUpdate()
{
	//ROS_INFO("Spatula PLUGIN RUNNING!!");
}

void SpatulaPlugin::SpatulaCallback(const hand_sim::HydraRaw& msg)
{
	if (msg.buttons[1] == 16)
	{
		this->hydraJointButtonStatus = true;
	}

	if(this->hydraJointButtonStatus && msg.buttons[1] == 0)
	{
		if (!this->jointCreated)
		{
			this->spatula_model->SetGravityMode(false);
			SpatulaPlugin::createJoint();
			this->jointCreated = true;
		}
		else
		{
			SpatulaPlugin::destroyJoint();
			this->spatula_model->SetGravityMode(true);
			this->jointCreated = false;
		}
		this->hydraJointButtonStatus = false;
	}
}

void SpatulaPlugin::initModels()
{
    this->hydraJointButtonStatus = false;
    this->jointCreated = false;

    if (this->spatula_model->GetWorld()->GetEntity("hit_hand") != NULL)
    {
    	this->rosie_model = this->spatula_model->GetWorld()->GetEntity("hit_hand")->GetParentModel();
    }
}

void SpatulaPlugin::createJoint()
{
	// Log Timestamp to file
	SpatulaPlugin::logTimestamp("creating joint with Spatula at:");

	math::Vector3 axis, direction;
	physics::LinkPtr spatula_link, palm_link;

	spatula_link = this->spatula_model->GetLink("spatula_link");
	palm_link = this->rosie_model->GetLink("right_hand_palm");

	direction = palm_link->GetWorldPose().pos - spatula_link->GetWorldPose().pos;
	direction.Normalize();
	axis = direction.Cross(math::Vector3(0.0, 0.0, 1.0));

	std::cout << "Creating joint with Spatula at : " << this->spatula_model->GetWorld()->GetSimTime() << std::endl;
	ROS_INFO("**CREATING JOINT** between links \"%s\" and \"%s\"",spatula_link->GetName().c_str(), palm_link->GetName().c_str());


	this->myJoint = this->spatula_model->GetWorld()->GetPhysicsEngine()->CreateJoint("revolute",  this->spatula_model);

	this->myJoint->Attach(spatula_link, palm_link);

	this->myJoint->Load(spatula_link, palm_link, math::Pose(spatula_link->GetWorldPose().pos, math::Quaternion()));

	this->myJoint->SetAxis(0, axis);

	this->myJoint->SetAttribute("stop_cfm",0, this->stop_cfm);
	this->myJoint->SetAttribute("stop_erp",0, this->stop_erp); // reducing the error reductions parameter allows joint to exceed the limit
	this->myJoint->SetAttribute("hi_stop",0, this->hi_stop);
	this->myJoint->SetAttribute("lo_stop",0, this->lo_stop);
}

void SpatulaPlugin::destroyJoint()
{
	// Log Timestamp to file
	SpatulaPlugin::logTimestamp("destroying joint with Spatula at:");

	std::cout << "Destroying joint with Spatula at : " << this->spatula_model->GetWorld()->GetSimTime() << std::endl;
	ROS_WARN("**DELETING JOINT** between links \"%s\" and \"%s\"",this->spatula_model->GetLink("spatula_link")->GetName().c_str(), this->rosie_model->GetLink("right_hand_palm")->GetName().c_str());
	this->myJoint->Detach();
}

void SpatulaPlugin::logTimestamp(std::string msg)
{
	  std::ofstream myfile;
	  myfile.open ("/home/haidu/.gazebo/log/joint_logs.txt", std::ios::out | std::ios::app);
	  myfile << msg.c_str()<< " sec " << this->spatula_model->GetWorld()->GetSimTime().sec << " nsec " << this->spatula_model->GetWorld()->GetSimTime().nsec << "\n";
	  myfile.close();
}

// Register this plugin with the simulator
  GZ_REGISTER_MODEL_PLUGIN(SpatulaPlugin)


