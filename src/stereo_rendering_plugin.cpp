#include "stereo_rendering_plugin.h"

using namespace gazebo;
using namespace rendering;



#define SCREEN_DIST 10
#define EYES_SPACING 0.06

#define EYESSPACING_SPEED 0.2
#define FOCALLENGTH_SPEED 2
#define SCALING_SPEED 2

Ogre::Camera* theCam;
Ogre::Entity* pPlanetEnt;

StereoManager *gStereoManager = NULL;
Ogre::Viewport *gLeftViewport = NULL;
Ogre::Viewport *gRightViewport = NULL;
Ogre::SceneManager *gSceneMgr;



// Register this plugin with the simulator
//GZ_REGISTER_WORLD_PLUGIN(StereoRenderingPlugin)
GZ_REGISTER_SYSTEM_PLUGIN(StereoRenderingPlugin)

StereoRenderingPlugin::StereoRenderingPlugin()
{
}

StereoRenderingPlugin::~StereoRenderingPlugin()
{
}

void StereoRenderingPlugin::Load(int /*_argc*/, char ** /*_argv*/)
{
	std::cout << "********** Stereo Rendering Plugin Started ********" << std::endl;
}

void StereoRenderingPlugin::Init()
{

	this->updateConnection = event::Events::ConnectPreRender(
			boost::bind(&StereoRenderingPlugin::OnUpdate, this));

	//common::Time::Sleep(1);
	std::cout << "********** In INIT ********" << std::endl;

	//this->mRoot = RenderEngine::Instance()->root;

	this->activeCam = gui::get_active_camera();

	this->mScene = this->activeCam->GetScene();

	gLeftViewport = this->activeCam->GetViewport();

	StereoManager::StereoMode mode = gStereoManager->SM_ANAGLYPH_RC;

/*	mStereoManager->init(gLeftViewport, gRightViewport, mode);*/

	mStereoManager->testFunction();

/*	this->offsetPose = math::Pose(math::Vector3(0,0,0.1), math::Quaternion(0,0,0,1));

	this->mRoot = RenderEngine::Instance()->root;

	this->activeCam = gui::get_active_camera();

	this->mScene = this->activeCam->GetScene();

	this->mScenemanager = this->mScene->GetManager();*/



	//this->offsetCam = this->activeCam;
	//this->offsetCam->SetWorldPose(this->activeCam->GetWorldPose() + this->offsetPose);
	//this->offsetCam->SetViewController("fps");
	//gui::set_active_camera(this->offsetCam);

	//this->userCamera->SetWorldPosition(math::Vector3(0,0,1.3));
	//this->scene = this->activeCam->GetScene();


/*	this->sleepDuration = common::Time(0, 0.008 * 1e9);*/

	// Create spinner thread in order to run the callbacks
	this->thread = new boost::thread(boost::bind(
			&StereoRenderingPlugin::ThreadFunc, this));



/*	mSceneMgr = this->mScene->GetManager();

	gLeftViewport = this->activeCam->GetViewport();*/

//	mStereoManager->init(gLeftViewport, gRightViewport, "stereo.cfg");

/*	try
	{
		// try to load the config file
		mStereoManager->init(gLeftViewport, gRightViewport, "stereo.cfg");
	}
	catch(Exception e)
	{
		// if the config file is not found, it throws an exception
		if(e.getNumber() == Exception::ERR_FILE_NOT_FOUND)
		{
			// init the manager with defaults parameters
			mStereoManager->init(gLeftViewport, gRightViewport, StereoManager::SM_DUALOUTPUT);
			mStereoManager->setEyesSpacing(EYES_SPACING);
			mStereoManager->setFocalLength(SCREEN_DIST);
			mStereoManager->saveConfig("stereo.cfg");
			//mStereoManager.useScreenWidth(SCREEN_WIDTH);
		}
		else
			throw e;
	}
	mStereoManager->createDebugPlane(mSceneMgr);
	mStereoManager->enableDebugPlane(false);



	gSceneMgr = mSceneMgr;
	gStereoManager = mStereoManager;

	// Check prerequisites first
	const RenderSystemCapabilities* caps = Root::getSingleton().getRenderSystem()->getCapabilities();
	if (!caps->hasCapability(RSC_VERTEX_PROGRAM) || !(caps->hasCapability(RSC_FRAGMENT_PROGRAM)))
	{
		OGRE_EXCEPT(1, "Your card does not support vertex and fragment programs, so cannot "
			"run this demo. Sorry!",
			"Fresnel::createScene");
	}
	else
	{
		if (!GpuProgramManager::getSingleton().isSyntaxSupported("arbfp1") &&
			!GpuProgramManager::getSingleton().isSyntaxSupported("ps_2_0") &&
			!GpuProgramManager::getSingleton().isSyntaxSupported("ps_1_4")
			)
		{
			OGRE_EXCEPT(1, "Your card does not support advanced fragment programs, "
				"so cannot run this demo. Sorry!",
				"Fresnel::createScene");
		}
	}*/



	std::cout << "********** Out of INIT ********" << std::endl;
}


void StereoRenderingPlugin::OnUpdate()
{

}


void StereoRenderingPlugin::ThreadFunc()
{
	std::cout << "********** In ThreadFunc ********" << std::endl;

	for(int i=0; i < 2000; ++i)
	{

		this->activeCam->SetWorldPosition(math::Vector3(0, 0.1 * (i%2), 1));
		common::Time::Sleep(this->sleepDuration);
	}

	std::cout << "********** Out ThreadFunc ********" << std::endl;
}












