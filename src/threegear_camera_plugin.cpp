#include "threegear_camera_plugin.h"

#define PI 3.14159

using namespace gazebo;
using namespace rendering;


// Register this plugin with the simulator
//GZ_REGISTER_WORLD_PLUGIN(ThreegearCameraPlugin)
GZ_REGISTER_SYSTEM_PLUGIN(ThreegearCameraPlugin)

ThreegearCameraPlugin::ThreegearCameraPlugin()
{
	this->control_flag = false;
	this->pinch_flag = false;

	int argc = 0;
	char** argv = NULL;
	ros::init(argc,argv,"threegear_camera_plugin");
	this->rosnode = new ros::NodeHandle();

	//this->threegear_pos_subscriber = this->rosnode->subscribe("/threegear_raw", 100, &ThreegearCameraPlugin::ThreegearPosCallback, this);

	this->threegear_gesture_subscriber = this->rosnode->subscribe("/threegear_gesture", 100, &ThreegearCameraPlugin::ThreegearGestureCallback, this);
}

ThreegearCameraPlugin::~ThreegearCameraPlugin()
{
	delete this->spinner_thread;
	delete this->rosnode;
}

void ThreegearCameraPlugin::Load(int /*_argc*/, char ** /*_argv*/)
{

	std::cout << "********** Threegear Camera Plugin Started ********" << std::endl;

	/*offset so the hand points forwards*/
	math::Vector3 a3;
	a3.x = 0;
	a3.y = PI/2;
	a3.z = PI/2;
	this->threegear_offset_q.SetFromEuler(a3);


}

void ThreegearCameraPlugin::Init()
{

	std::cout << "********** In INIT ********" << std::endl;
	this->activeCam = gui::get_active_camera();

	this->activeCam->SetWorldPosition(math::Vector3(-1,0,1));

	// Create spinner thread in order to run the callbacks
	this->spinner_thread = new boost::thread(boost::bind(&ThreegearCameraPlugin::SpinnerFunc, this));

	std::cout << "********** Out of INIT ********" << std::endl;

}

void ThreegearCameraPlugin::ThreegearGestureCallback(const hand_sim::ThreegearGesture& msg)
{
	//std::cout << "********** Threegear Gesture Callback ********" << std::endl;

	if (msg.gesture_left == 2)
	{
		this->pinch_flag = true;
	}

	if (this->pinch_flag && msg.gesture_left !=2)
	{
		this->control_flag = !this->control_flag;
		if(this->control_flag)
		{
			this->activeCam->SetViewController("fps");
			std::cout << "********** FPS ********" << std::endl;
		}
		else
		{
			this->activeCam->SetViewController("orbit");
			std::cout << "********** ORBIT ********" << std::endl;
		}

		this->pinch_flag = false;

		//gui::set_active_camera(this->activeCam);
	}




}

void ThreegearCameraPlugin::ThreegearPosCallback(const hand_sim::ThreegearPosition& msg)
{
	//std::cout << "********** Threegear Callback ********" << std::endl;

/*	if (this->pinch_flag)
	{
		return;
	}*/

/*	math::Vector3 camera_pos;
	camera_pos.x = msg.pos_left[2]/100;
	camera_pos.y = msg.pos_left[0]/100;
	camera_pos.z = msg.pos_left[1]/100;

	math::Quaternion q_camera_msg;
	q_camera_msg.Set( 0,0,0,0);
	//q_camera_msg.Set( msg.rot_left[0], - msg.rot_left[3], - msg.rot_left[1], msg.rot_left[2]);
	q_camera_msg.Normalize();

	cameraPose.rot = q_camera_msg * this->threegear_offset_q;
	cameraPose.rot.Normalize();
	cameraPose.pos = camera_pos;

	this->activeCam->SetWorldPose(cameraPose);*/
}

void ThreegearCameraPlugin::SpinnerFunc()
{
	std::cout << "********** In SpinnerFunc ********" << std::endl;
	//ros::spinOnce();

	ros::spin();
	std::cout << "********** Out SpinnerFunc ********" << std::endl;
}











