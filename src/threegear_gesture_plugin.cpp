#include "threegear_gesture_plugin.h"
#include <boost/tokenizer.hpp>

#define DRAGGED 2
#define RELEASED 3

using namespace gazebo;

ThreegearGesturePlugin::ThreegearGesturePlugin()
{
	int argc = 0;
	char** argv = NULL;
	ros::init(argc,argv,"Threegear_gesture_plugin");

}

ThreegearGesturePlugin::~ThreegearGesturePlugin()
{
	delete this->rosnode;
	this->contactNode->Fini();
}


//////////////////////////////////////////////////////////////////////////////////////
void ThreegearGesturePlugin::Load(physics::ModelPtr _parent, sdf::ElementPtr /*_sdf*/)
{
	// Store the pointer to the model
	this->hand_model = _parent;

	// Initialize the world
	this->world = _parent->GetWorld();

	this->rosnode = new ros::NodeHandle();

	// Subscriber to threegear gesture topic
	this->threegear_gesture_subscriber = this->rosnode->subscribe("/threegear_gesture", 10, &ThreegearGesturePlugin::ThreegearGestureCallback, this);

	// Listen to the update event. This event is broadcast every simulation iteration.
	this->updateConnection = event::Events::ConnectWorldUpdateBegin(boost::bind(&ThreegearGesturePlugin::OnUpdate, this));

	ROS_INFO("********THREEGEAR GESTURE PLUGIN STARTED*********");
}


//////////////////////////////////////////////////////////////////////////////////////
void ThreegearGesturePlugin::Init()
{
	// Set thumb joint
    this->thumb_base_joint = this->hand_model->GetJoint("right_hand_thumb_joint");

	// Gripper mode
    this->thumb_base_joint->SetAngle(0, 1.57);

    // Set hand joints
    ThreegearGesturePlugin::setHandJoints();

    //Set up contact sensor listener for thumb finger tip
    ThreegearGesturePlugin::initContactNode();

    // Init global variables
	this->gesture = -1;
	this->fixed_joint_attached = false;
	this->gripper_button_pressed = false;
}


//////////////////////////////////////////////////////////////////////////////////////
void ThreegearGesturePlugin::OnUpdate()
{
	ros::spinOnce();
}


//////////////////////////////////////////////////////////////////////////////////////
void ThreegearGesturePlugin::initContactNode()
{
	// create listener to the contact topic, and callback OnContact
	this->contactNode = transport::NodePtr(new transport::Node());

	this->contactNode->Init();

	this->contactSub = this->contactNode->Subscribe("~/thumb_contact", &ThreegearGesturePlugin::OnContact, this);

	this->fixed_joint = this->world->GetPhysicsEngine()->CreateJoint(
			"revolute", this->hand_model);
}

//////////////////////////////////////////////////////////////////////////////////////
void ThreegearGesturePlugin::ThreegearGestureCallback(const hand_sim::ThreegearGesture& msg)
{
	this->gesture = msg.gesture_right;

	// Closing Hand
	if (this->gesture == DRAGGED)
	{
	 	this->gripper_button_pressed = true;
		// Close / Open gripper
		if (!this->fixed_joint_attached)
		{
			// Close gripper
			ThreegearGesturePlugin::CloseGripper(true);
		}
		else
		{
			// Keep fingers in position
			ThreegearGesturePlugin::CloseGripper(false);
		}
	}
	// Opening Hand
	else
	{
	 	this->gripper_button_pressed = false;
		ThreegearGesturePlugin::OpenGripper();
	}
}

//////////////////////////////////////////////////////////////////////////////////////
void ThreegearGesturePlugin::setHandJoints()
{
    this->hand_joints.push_back(this->hand_model->GetJoint("right_hand_thumb_proximal_joint"));
    this->hand_joints.push_back(this->hand_model->GetJoint("right_hand_thumb_middle_joint"));
    this->hand_joints.push_back(this->hand_model->GetJoint("right_hand_thumb_distal_joint"));
}


//////////////////////////////////////////////////////////////////////////////////////
void ThreegearGesturePlugin::attachJoint(const physics::LinkPtr attachLink)
{
	physics::LinkPtr palm_link = this->hand_model->GetLink("right_hand_palm");

	std::cout << "Creating joint between " << palm_link->GetName().c_str()
			<< " and " << attachLink->GetName().c_str() << std::endl;

	this->fixed_joint->Load(palm_link, attachLink, math::Pose());

    this->fixed_joint->Init();
    this->fixed_joint->SetHighStop(0, 0);
    this->fixed_joint->SetLowStop(0, 0);

	this->fixed_joint_attached = true;
}


//////////////////////////////////////////////////////////////////////////////////////
void ThreegearGesturePlugin::detachJoint()
{
	std::cout << "Releasing object! " <<  std::endl;
	this->fixed_joint->Detach();
	this->fixed_joint_attached = false;
}


//////////////////////////////////////////////////////////////////////////////////////
void ThreegearGesturePlugin::OnContact(ConstContactsPtr &_msg)
{
	if (this->gripper_button_pressed && _msg->contact_size() > 0 && !this->fixed_joint_attached)
	{
		// open gripper before attaching the joint so there is no actual contact
		// the object acts weird if there is an actual contact
		ThreegearGesturePlugin::OpenGripper();

		//std::cout << "collision1: " << _msg->contact(_msg->contact_size()-1).collision1().c_str() << std::endl;
		//std::cout << "collision2: " << _msg->contact(_msg->contact_size()-1).collision2().c_str() << std::endl;

		boost::char_separator<char> sep("::");

		boost::tokenizer< boost::char_separator<char> > tok(_msg->contact(0).collision1(), sep);
		boost::tokenizer< boost::char_separator<char> >::iterator beg=tok.begin();

		physics::ModelPtr attachModel = this->world->GetModel(*beg);
		//std::cout << "attach Model: " << attachModel->GetName().c_str() << std::endl;
		beg++;
		physics::LinkPtr attachLink = attachModel->GetLink(*beg);
		//std::cout << "attach Link: " << attachLink->GetName().c_str() << std::endl;

		ThreegearGesturePlugin::attachJoint(attachLink);
	}
	else if(!this->gripper_button_pressed && this->fixed_joint_attached)
	{
		ThreegearGesturePlugin::detachJoint();

	}
}

//////////////////////////////////////////////////////////////////////////////////////
void ThreegearGesturePlugin::CloseGripper(bool close_flag)
{
	if(close_flag)
	{
		//std::cout << "closing" << std::endl;
		const double force = 60000;
		// only thumb
		this->hand_joints[0]->SetForce(0, force);
		this->hand_joints[1]->SetForce(0, force/4);
		this->hand_joints[2]->SetForce(0, force/8);
	}
}

//////////////////////////////////////////////////////////////////////////////////////
void ThreegearGesturePlugin::OpenGripper()
{
	if (this->hand_joints[0]->GetAngle(0).Radian() > 0.1)
	{
		//std::cout << "opening" << std::endl;
		const double force = 60000;
		// only thumb
		this->hand_joints[0]->SetForce(0, - force/2);
		this->hand_joints[1]->SetForce(0, - force);
		this->hand_joints[2]->SetForce(0, - force);
	}
}

// Register this plugin with the simulator
  GZ_REGISTER_MODEL_PLUGIN(ThreegearGesturePlugin)


