#include "threegear_plugin.h"

#define PI 3.14159

using namespace gazebo;

ThreegearPlugin::ThreegearPlugin()
{
	  int argc = 0;
	  char** argv = NULL;
	  ros::init(argc,argv,"Threegear_plugin");
      // ROS Nodehandle
	  this->rosnode = new ros::NodeHandle();

	  // ThreeGear Subscriber
	  this->threegear_subscriber = this->rosnode->subscribe("/threegear_raw", 100, &ThreegearPlugin::ThreegearCallback, this);

}

ThreegearPlugin::~ThreegearPlugin()
{
	delete this->rosnode;
}

void ThreegearPlugin::Load(physics::ModelPtr _parent, sdf::ElementPtr /*_sdf*/)
    {
      // Store the pointer to the model
      this->hand_model = _parent;

      // Listen to the update event. This event is broadcast every simulation iteration.
      this->updateConnection = event::Events::ConnectWorldUpdateBegin(boost::bind(&ThreegearPlugin::OnUpdate, this));

      //disable gravity on the hand model
      this->hand_model->SetGravityMode(false);

      //get current time of the simulation
      this->time_of_last_cycle = this->hand_model->GetWorld()->GetSimTime();

      this->pid_controller_x.Reset();
      this->pid_controller_y.Reset();
      this->pid_controller_z.Reset();

      /*initialize translation PID*/
      ThreegearPlugin::InitPid(7, 0, 10.0, 10, -10);

      /*set initial desired position of the hand (the spawned position)*/
      this->curr_position = this->hand_model->GetWorldPose().pos;
      this->desired_position.x = this->curr_position.x;
      this->desired_position.y = this->curr_position.y;
      this->desired_position.z = this->curr_position.z;

      /*set the initial value of the rotation*/
      this->desired_quatern.Set(0.0, 0.0, 0.0, 0.0);

      ThreegearPlugin::ThreegearInit();

      ROS_INFO("********THREEGEAR PLUGIN STARTED*********");

    }

    // Called by the world update start event
void ThreegearPlugin::OnUpdate()
    {
		ros::spinOnce();

		/* current pose of the model*/
		this->curr_pose = this->hand_model->GetWorldPose();
		this->curr_quatern = curr_pose.rot;
		this->curr_position = curr_pose.pos;

		/*delta time between cycles*/
		common::Time dt = (double) this->hand_model->GetWorld()->GetSimTime().Double() - this->time_of_last_cycle.Double();
		this->time_of_last_cycle = this->hand_model->GetWorld()->GetSimTime();

	    /*computing lin/rot velocities*/
		this->pid_lin_velocity.x = pid_controller_x.Update(this->curr_position.x - this->desired_position.x, dt);
		this->pid_lin_velocity.y = pid_controller_y.Update(this->curr_position.y - this->desired_position.y, dt);
		this->pid_lin_velocity.z = pid_controller_z.Update(this->curr_position.z - this->desired_position.z, dt);

		this->rot_velocity = ThreegearPlugin::returnRotVelocity();

		/*apply the computed lin/rot forces/velocities to the model*/
		//this->hand_model->GetLinkById(0)->SetForce(this->pid_lin_velocity);
		this->hand_model->GetLink("right_hand_palm")->SetForce(this->pid_lin_velocity);
		this->hand_model->SetAngularVel(this->rot_velocity);

    }


void ThreegearPlugin::InitPid(double P, double I, double D, double I1_max,  double I2_min)
{
	  this->pid_controller_x.Init(P, I, D, I1_max, I2_min);
	  this->pid_controller_y.Init(P, I, D, I1_max, I2_min);
	  this->pid_controller_z.Init(P, I, D, I1_max, I2_min);
}

void ThreegearPlugin::ThreegearInit()
{
	/*offset so the hand points forwards*/
	math::Vector3 a3;
	a3.x = 0;
	a3.y = PI/2;
	a3.z = PI/2;
	this->threegear_offset_q.SetFromEuler(a3);

	this->offset_position.x = 0.1;
	this->offset_position.y = 0;
	this->offset_position.z = 0.6;
}

math::Vector3 ThreegearPlugin::returnRotVelocity()
{
    math::Vector3 rot_velocity;
    math::Quaternion quatern_diff, vel_q;

     /* Check if dot product is negative or positive so it doesn't jump if it passes PI/2 position
      * in case dot product is negative, change the des_q values to negative */
    if(((this->desired_quatern.x * this->curr_quatern.x) +
    	(this->desired_quatern.y * this->curr_quatern.y) +
    	(this->desired_quatern.y * this->curr_quatern.y)) > 0)
    	{
    		quatern_diff = (this->desired_quatern - this->curr_quatern)*2.0;
    		vel_q = quatern_diff * this->curr_quatern.GetInverse();
    	}
    else
    	{
			quatern_diff = ( - this->desired_quatern - this->curr_quatern)*2.0;
			vel_q = quatern_diff * this->curr_quatern.GetInverse();
    	}

    rot_velocity.x = vel_q.x*22;
    rot_velocity.y = vel_q.y*22;
    rot_velocity.z = vel_q.z*22;

    return rot_velocity;

}

void ThreegearPlugin::ThreegearCallback(const hand_sim::ThreegearPosition& msg)
{
	  math::Quaternion q_msg;

	  q_msg.Set((float)msg.rot_right[0], - (float)msg.rot_right[3], - (float)msg.rot_right[1], (float)msg.rot_right[2]);

	  this->desired_position.x = - (float) msg.pos_right[2]/400 + this->offset_position.x;
	  this->desired_position.y = - (float) msg.pos_right[0]/400 + this->offset_position.y;
	  this->desired_position.z = (float) msg.pos_right[1]/400 + this->offset_position.z;

	  this->desired_quatern = q_msg * this->threegear_offset_q;

}

// Register this plugin with the simulator
  GZ_REGISTER_MODEL_PLUGIN(ThreegearPlugin)


