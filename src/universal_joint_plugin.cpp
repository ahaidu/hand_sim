#include "universal_joint_plugin.h"

using namespace gazebo;

UniversalJointPlugin::UniversalJointPlugin()
{
	  int argc = 0;
	  char** argv = NULL;
	  ros::init(argc,argv,"Universal_plugin");
	  this->rosnode = new ros::NodeHandle();

	  this->hydra_subscriber = this->rosnode->subscribe("/hydra_raw", 10, &UniversalJointPlugin::UniversalCallback, this);
}

UniversalJointPlugin::~UniversalJointPlugin()
{
}

void UniversalJointPlugin::Load(physics::ModelPtr _parent, sdf::ElementPtr /*_sdf*/)
    {
      // Store the pointer to the model
      this->universal_model = _parent;

      // Listen to the update event. This event is broadcast every simulation iteration.
      //this->updateConnection = event::Events::ConnectWorldUpdateBegin(boost::bind(&UniversalJointPlugin::OnUpdate, this));

      UniversalJointPlugin::initModels();

      ROS_INFO("********UNIVERSAL PLUGIN STARTED*********");
    }

    // Called by the world update start event
void UniversalJointPlugin::OnUpdate()
    {
		//ROS_INFO("Universal PLUGIN RUNNING!!");
    }

void UniversalJointPlugin::UniversalCallback(const hand_sim::HydraRaw& msg)
{
	if (msg.buttons[1] == 16)
	{
		this->hydraJointButtonStatus = true;
	}

	if(this->hydraJointButtonStatus && msg.buttons[1] == 0)
	{
		if (!this->jointCreated)
		{
			this->universal_model->SetGravityMode(false);
			UniversalJointPlugin::createJoint();
			this->jointCreated = true;
		}
		else
		{
			UniversalJointPlugin::destroyJoint();
			this->universal_model->SetGravityMode(true);
			this->jointCreated = false;
		}
		this->hydraJointButtonStatus = false;
	}
}

void UniversalJointPlugin::initModels()
{
    this->hydraJointButtonStatus = false;
    this->jointCreated = false;

    if (this->universal_model->GetWorld()->GetEntity("hit_hand") != NULL)
    {
    	// init rosie_model if we have the hit hand in the world
    	this->rosie_model = this->universal_model->GetWorld()->GetEntity("hit_hand")->GetParentModel();
    }
}

void UniversalJointPlugin::createJoint()
{
	// Log Timestamp to file
	//UniversalJointPlugin::logTimestamp("creating joint with Universal at:");

	ROS_INFO("**CREATING JOINT** between links \"%s\" and \"%s\"",this->universal_model->GetLink("spoon_link")->GetName().c_str(), this->rosie_model->GetLink("right_hand_palm")->GetName().c_str());

	this->myJoint = this->universal_model->GetWorld()->GetPhysicsEngine()->CreateJoint("revolute", this->universal_model);

	this->myJoint->Attach(this->universal_model->GetLink("spoon_link"),this->rosie_model->GetLink("right_hand_palm"));

	this->myJoint->SetAttribute("hi_stop",0, 0.0);
	this->myJoint->SetAttribute("lo_stop",0,-0.0);
}

void UniversalJointPlugin::destroyJoint()
{
	// Log Timestamp to file
	//UniversalJointPlugin::logTimestamp("destroying joint with Universal at:");
	ROS_WARN("**DELETING JOINT** between links \"%s\" and \"%s\"",this->universal_model->GetLink("spoon_link")->GetName().c_str(), this->rosie_model->GetLink("right_hand_palm")->GetName().c_str());
	this->myJoint->Detach();
}

void UniversalJointPlugin::logTimestamp(std::string msg)
{
	  std::ofstream myfile;
	  myfile.open ("/home/haidu/.gazebo/log/joint_logs.txt", std::ios::out | std::ios::app);
	  myfile << msg.c_str()<< " sec " << this->universal_model->GetWorld()->GetSimTime().sec << " nsec " << this->universal_model->GetWorld()->GetSimTime().nsec << "\n";
	  myfile.close();
}

// Register this plugin with the simulator
  GZ_REGISTER_MODEL_PLUGIN(UniversalJointPlugin)


